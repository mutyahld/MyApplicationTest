package com.example.muthiahalida.myapplicationtest.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.muthiahalida.myapplicationtest.R;
import com.example.muthiahalida.myapplicationtest.model.Notification;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Muthia Halida on 09/03/2019.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    private Context context;
    private List<Notification> notificationList;

    public NotificationAdapter(Context context, List<Notification> notificationList) {
        this.context = context;
        this.notificationList = notificationList;
    }

    public List<Notification> getNotificationList() {
        return notificationList;
    }

    @NonNull

    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationAdapter.NotificationViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.NotificationViewHolder holder, int position) {
        holder.bind(getNotificationList().get(position));
    }

    @Override
    public int getItemCount() {
        return getNotificationList().size();
    }


    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_notification)
        TextView tvNotification;
        @BindView(R.id.tv_tanggal)
        TextView tvTanggal;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Notification data) {
            tvTanggal.setText(data.getCurrentTime());
            tvNotification.setText(data.getNotification());
        }
    }
}
