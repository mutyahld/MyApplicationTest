package com.example.muthiahalida.myapplicationtest;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.muthiahalida.myapplicationtest.model.News;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFagment extends Fragment {


    @BindView(R.id.iv_image)
    ImageView ivImage;
    Unbinder unbinder;

    News news;

    public NewsFagment() {
        // Required empty public constructor
    }

    public static NewsFagment start(News news) {
        Bundle args = new Bundle();
        args.putParcelable("news", news);
        NewsFagment newsFagment = new NewsFagment();
        newsFagment.setArguments(args);
        return newsFagment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        news = getArguments().getParcelable("news");

        Glide.with(getContext()).load(news.getUrl()).into(ivImage);
        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetCommentFragment bottomSheetCommentFragment=BottomSheetCommentFragment.getInstance(news);
                bottomSheetCommentFragment.show(getFragmentManager(),"comment fragment");
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
