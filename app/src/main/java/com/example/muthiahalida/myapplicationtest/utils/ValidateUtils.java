package com.example.muthiahalida.myapplicationtest.utils;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;

/**
 * Created by Muthia Halida on 16/01/2019.
 */

public class ValidateUtils {

    public static void validateInputUser(final TextInputLayout[] textInputLayouts, final Button button){

        for (final TextInputLayout textInputLayout : textInputLayouts) {
            textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (TextUtils.isEmpty(charSequence)) {
                        textInputLayout.setError("diharuskan isi");
                    } else {
                        textInputLayout.setErrorEnabled(false);
                    }
                    validateButtonEnabled(textInputLayouts, button);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

    public static void validateButtonEnabled(TextInputLayout[] textInputLayouts, final Button button){
        boolean isValid = true;

        for (TextInputLayout textInputLayout: textInputLayouts){
            if (TextUtils.isEmpty(textInputLayout.getEditText().getText())){
                isValid = false;
            }
        }

        button.setEnabled(isValid);
    }
}
