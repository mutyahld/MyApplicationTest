package com.example.muthiahalida.myapplicationtest.utils;

import com.google.firebase.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static String toSpecifictMonthInCalendar(Timestamp date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(date.getSeconds()*1000));
        return String.valueOf(calendar.get(Calendar.MONTH));
    }

    public static Date toLocalDate(Timestamp date){
        return new Date(date.getSeconds()*1000);
    }

    public static Timestamp toFirestoreTimestamp(Date date){
        return new Timestamp(date);
    }

}
