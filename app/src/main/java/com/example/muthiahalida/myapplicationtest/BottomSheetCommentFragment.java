package com.example.muthiahalida.myapplicationtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.muthiahalida.myapplicationtest.Adapter.CommentAdapter;
import com.example.muthiahalida.myapplicationtest.Adapter.CommentAdapter.OnDeleteCommentClickListener;
import com.example.muthiahalida.myapplicationtest.custom.MyRecyclerView;
import com.example.muthiahalida.myapplicationtest.model.Comment;
import com.example.muthiahalida.myapplicationtest.model.News;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Nullable;


public class BottomSheetCommentFragment extends BottomSheetDialogFragment implements OnDeleteCommentClickListener {

    Unbinder unbinder;

    @BindView(R.id.tv_comment)
    TextView tvComment;

    @BindView(R.id.rv_bottom_sheet_comment)
    MyRecyclerView rvBottomSheetComment;

    @BindView(R.id.et_comment)
    EditText etComment;

    @BindView(R.id.btn_comment)
    Button btnComment;

    private List<Comment> Comments;

    private CommentAdapter adapter;

    private FirebaseFirestore firebaseFirestore;

    private FirebaseUser firebaseUser;

    private String name;

    private String photoUrl;

    private String comment;

    private Comment newComment;

    private Comment mComment;

    private Timestamp currentTime;

    private News news;

    private String idNews;

    private String idUser;

    public static BottomSheetCommentFragment getInstance() {
        return new BottomSheetCommentFragment();
        //instance=intennya fragment
    }

    public static BottomSheetCommentFragment getInstance(News news) {
        BottomSheetCommentFragment fragment = new BottomSheetCommentFragment();
        Bundle args = new Bundle();
        args.putParcelable("news", news);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_comment, container, false);
        unbinder = ButterKnife.bind(this, view);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        idUser = firebaseUser.getUid();

        if (getArguments() != null) {
            news = getArguments().getParcelable("news");
            idNews = news.getId();
        }

        getCommentList();

        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserData();
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCommentDeleted(final Comment comment) {
        firebaseFirestore.collection("news")
                .document(idNews)
                .collection("Comment")
                .document(comment.getIdComment())
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull final Task<Void> task) {
                        Toast.makeText(getContext(),"Komentar Dihapus", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getUserData() {
        firebaseFirestore.collection("User")
                .document(idUser)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        name = String.valueOf(documentSnapshot.get("fullName"));
                        photoUrl = String.valueOf(documentSnapshot.get("uploadPhoto"));
                        saveCommentUser();
                    }
                });
    }

    private void saveCommentUser() {
        getCurrentTime();
        comment = etComment.getText().toString();
        newComment = new Comment(idUser, photoUrl, name, comment, currentTime);
        firebaseFirestore.collection("news")
                .document(idNews)
                .collection("Comment")
                .add(newComment)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        WriteBatch batch = firebaseFirestore.batch();
                        String idcomment = documentReference.getId();
                        newComment = new Comment(idcomment, idUser, photoUrl, name, comment, currentTime);
                        batch.update(documentReference, "idComment", idcomment);
                        batch.commit()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull final Task<Void> task) {
                                        onCommentSuccess();
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void onCommentSuccess() {
        etComment.setText("");
    }

    private void getCurrentTime() {
        Date date = new Date();
        currentTime = new Timestamp(date);
    }

    private void getCommentList() {
        firebaseFirestore.collection("news")
                .document(idNews)
                .collection("Comment")
                .orderBy("currentTime", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                            @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {
                            Comments = new ArrayList<>();
                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                mComment = documentSnapshot.toObject(Comment.class);
                                if (mComment.getIdUser() != null) {
                                    if (mComment.getIdUser().equals(idUser)) {
                                        mComment.setFromUser(true);
                                    } else {
                                        mComment.setFromUser(false);
                                    }
                                    Comments.add(mComment);
                                }
                            }
                            setRvBottomSheetComment();
                        }
                    }
                });
    }

    private void setRvBottomSheetComment() {
        adapter = new CommentAdapter(getContext(), Comments);
        rvBottomSheetComment.setLayoutManager(new LinearLayoutManager(getContext()));
        rvBottomSheetComment
                .addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rvBottomSheetComment.setAdapter(adapter);
        rvBottomSheetComment.setVerticalScrollBarEnabled(false);
        adapter.setOnDeleteCommentClickListener(this);
        //kenapa dia getContext bukan this? karena dia fragment
    }
}
