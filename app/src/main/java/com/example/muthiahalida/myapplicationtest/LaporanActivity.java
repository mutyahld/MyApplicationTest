package com.example.muthiahalida.myapplicationtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.model.Laporan;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LaporanActivity extends AppCompatActivity {

    FirebaseFirestore firebaseFirestore;

    @BindView(R.id.sv_laporan)
    ScrollView svLaporan;

    @BindView(R.id.tl_laporan)
    TableLayout tlLaporan;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tr_laporan)
    TableRow trLaporan;

    @BindView(R.id.tv_judul_data_laporan)
    TextView tvJudulDataLaporan;

    @BindView(R.id.tv_keterangan)
    TextView tvKeterangan;

    @BindView(R.id.tv_no)
    TextView tvNo;

    @BindView(R.id.tv_pagu_anggaran)
    TextView tvPaguAnggaran;

    @BindView(R.id.tv_realisasi_keu_dana)
    TextView tvRealisasiKeuDana;

    @BindView(R.id.tv_progres_fisik)
    TextView tvProgresFisik;

    @BindView(R.id.tv_unit_organisasi)
    TextView tvUnitOrganisasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Laporan");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        firebaseFirestore = FirebaseFirestore.getInstance();

        getLaporanList();

    }

    private void getLaporanList() {
        firebaseFirestore.collection("laporan")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {
                            List<Laporan> laporanList = new ArrayList<>();

                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                Laporan laporan = documentSnapshot.toObject(Laporan.class);
                                laporanList.add(laporan);
                            }

                            laporanBasedOnOrganisasi(laporanList);
                        }
                    }
                });
    }

    private void laporanBasedOnOrganisasi(List<Laporan> laporanList) {
        HashMap<String, List<Laporan>> laporanOrganisasiMap = new HashMap<>();

        for (Laporan laporan : laporanList) {
            String nmUnit = laporan.getNmUnit();
            if (!laporanOrganisasiMap.containsKey(nmUnit)) {
                List<Laporan> laporanPerSatker = new ArrayList<>();
                laporanPerSatker.add(laporan);
                laporanOrganisasiMap.put(nmUnit, laporanPerSatker);
            } else {
                laporanOrganisasiMap.get(nmUnit).add(laporan);
            }
        }

        List<String> orgKeys = new ArrayList<>(laporanOrganisasiMap.keySet());
        int no = 0;
        for (String key : orgKeys) {
            no += 1;
            List<Laporan> laporans = laporanOrganisasiMap.get(key);
            setDataTable(no, laporans);
        }
    }

    private void setDataTable(int no, List<Laporan> laporanList) {
        long realisasi = 0;
        long progressFisik = 0;
        String nmUnit = "";
        long paguAnggaran = 0;
        String keterangan = "";
        for (Laporan laporan:laporanList){
            nmUnit = laporan.getNmUnit();
            paguAnggaran = laporan.getTotalPagu();
            realisasi += laporan.getRealisasiKeuDana();
            progressFisik += laporan.getProgresFisik();
            keterangan = laporan.getKeterangan();
        }

        TableRow row = new TableRow(this);

        TextView tvNo = new TextView(this);
        tvNo.setText(String.valueOf(no));
        tvNo.setGravity(Gravity.CENTER);
        tvNo.setBackground(getResources().getDrawable(R.drawable.border5));
        tvNo.setPadding(10, 10, 10, 10);
        row.addView(tvNo);

        TextView tvUnitOrganisasi = new TextView(this);
        tvUnitOrganisasi.setText(nmUnit);
        tvUnitOrganisasi.setGravity(Gravity.CENTER);
        tvUnitOrganisasi.setBackground(getResources().getDrawable(R.drawable.border5));
        tvUnitOrganisasi.setPadding(10, 10, 10, 10);
        row.addView(tvUnitOrganisasi);

        TextView tvPaguAnggaran = new TextView(this);
        tvPaguAnggaran.setText(String.valueOf(paguAnggaran));
        tvPaguAnggaran.setGravity(Gravity.CENTER);
        tvPaguAnggaran.setBackground(getResources().getDrawable(R.drawable.border5));
        tvPaguAnggaran.setPadding(10, 10, 10, 10);
        row.addView(tvPaguAnggaran);

        TextView tvRealisasiKeuangan = new TextView(this);
        tvRealisasiKeuangan.setText(String.valueOf(realisasi));
        tvRealisasiKeuangan.setGravity(Gravity.CENTER);
        tvRealisasiKeuangan.setBackground(getResources().getDrawable(R.drawable.border5));
        tvRealisasiKeuangan.setPadding(10, 10, 10, 10);
        row.addView(tvRealisasiKeuangan);

        TextView tvProgresFisik = new TextView(this);
        tvProgresFisik.setText(String.valueOf(progressFisik));
        tvProgresFisik.setGravity(Gravity.CENTER);
        tvProgresFisik.setBackground(getResources().getDrawable(R.drawable.border5));
        tvProgresFisik.setPadding(10, 10, 10, 10);
        row.addView(tvProgresFisik);

        TextView tvKeterangan = new TextView(this);
        tvKeterangan.setText(keterangan);
        tvKeterangan.setGravity(Gravity.CENTER);
        tvKeterangan.setBackground(getResources().getDrawable(R.drawable.border5));
        tvKeterangan.setPadding(10, 10, 10, 10);
        row.addView(tvKeterangan);

        tlLaporan.addView(row);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}