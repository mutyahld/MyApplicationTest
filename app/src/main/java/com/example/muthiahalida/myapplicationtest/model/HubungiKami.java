package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 06/09/2019.
 */

public class HubungiKami {
    private String dokumen;
    private String isi;

    public HubungiKami() {
    }

    public String getDokumen() {
        return dokumen;
    }

    public void setDokumen(String dokumen) {
        this.dokumen = dokumen;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
