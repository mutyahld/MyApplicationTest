package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 09/02/2019.
 */

public class UnitOrganisasi {
    private String name;
    private String id;
    private long paguBelanjaBarang;
    private long paguBelanjaModal;
    private long paguBelanjaPegawai;
    private long totalPagu;
    private long totalSatker;

    public UnitOrganisasi() {
    }

    public UnitOrganisasi(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public UnitOrganisasi(final String name, final String id, final long paguBelanjaBarang,
            final long paguBelanjaModal, final long paguBelanjaPegawai, final long totalPagu) {
        this.name = name;
        this.id = id;
        this.paguBelanjaBarang = paguBelanjaBarang;
        this.paguBelanjaModal = paguBelanjaModal;
        this.paguBelanjaPegawai = paguBelanjaPegawai;
        this.totalPagu = totalPagu;
    }

    public UnitOrganisasi(final String name, final String id, final long paguBelanjaBarang,
            final long paguBelanjaModal, final long paguBelanjaPegawai,
            final long totalPagu, final long totalSatker) {
        this.name = name;
        this.id = id;
        this.paguBelanjaBarang = paguBelanjaBarang;
        this.paguBelanjaModal = paguBelanjaModal;
        this.paguBelanjaPegawai = paguBelanjaPegawai;
        this.totalPagu = totalPagu;
        this.totalSatker = totalSatker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getPaguBelanjaBarang() {
        return paguBelanjaBarang;
    }

    public void setPaguBelanjaBarang(final long paguBelanjaBarang) {
        this.paguBelanjaBarang = paguBelanjaBarang;
    }

    public long getPaguBelanjaModal() {
        return paguBelanjaModal;
    }

    public void setPaguBelanjaModal(final long paguBelanjaModal) {
        this.paguBelanjaModal = paguBelanjaModal;
    }

    public long getPaguBelanjaPegawai() {
        return paguBelanjaPegawai;
    }

    public void setPaguBelanjaPegawai(final long paguBelanjaPegawai) {
        this.paguBelanjaPegawai = paguBelanjaPegawai;
    }

    public long getTotalPagu() {
        return totalPagu;
    }

    public void setTotalPagu(final long totalPagu) {
        this.totalPagu = totalPagu;
    }

    public long getTotalSatker() {
        return totalSatker;
    }

    public void setTotalSatker(final long totalSatker) {
        this.totalSatker = totalSatker;
    }
}
