package com.example.muthiahalida.myapplicationtest;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.model.Bulan;
import com.example.muthiahalida.myapplicationtest.model.Laporan;
import com.example.muthiahalida.myapplicationtest.model.Satker;
import com.example.muthiahalida.myapplicationtest.model.UnitOrganisasi;
import com.example.muthiahalida.myapplicationtest.utils.ValidateUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query.Direction;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Nullable;

public class UploadLaporanActivity extends AppCompatActivity {

    @BindView(R.id.et_keterangan)
    TextInputLayout etKeterangan;

    String keterangan;

    long realisasiKeuangan;

    long progresFisik;

    long totalPagu;

    String satker;

    String unitOrganisasi;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_pilih_satuan_kerja)
    TextView tvPilihSatuanKerja;

    @BindView(R.id.spinner_satker)
    AppCompatSpinner spinnerSatker;

    @BindView(R.id.tv_pilih_unit_organisasi)
    TextView tvPilihUnitOrganisasi;

    @BindView(R.id.spinner_unit_organisasi)
    AppCompatSpinner spinnerUnitOrganisasi;

    @BindView(R.id.et_realisasi_keu_dana)
    TextInputLayout etRealisasiKeuDana;

    @BindView(R.id.et_progres_fisik)
    TextInputLayout etProgresFisik;

    @BindView(R.id.btn_upload_laporan)
    Button btnUploadLaporan;


    private List<Satker> satkers;

    private List<UnitOrganisasi> unitOrganisasis;

    private List<Bulan> bulans;

    private Satker msatker;

    private FirebaseFirestore firebaseFirestore;

    private TextInputLayout[] textInputLayouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_laporan);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        firebaseFirestore = FirebaseFirestore.getInstance();

        textInputLayouts = new TextInputLayout[]{
                etKeterangan, etRealisasiKeuDana, etProgresFisik,
        };

        getSatkerList();
        getUnitOrganisasiList();

        ValidateUtils.validateInputUser(textInputLayouts, btnUploadLaporan);

        btnUploadLaporan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (spinnerUnitOrganisasi.getSelectedItemPosition() == 0
                        && spinnerSatker.getSelectedItemPosition() == 0) {
                    return;
                } else {
                    satker = spinnerSatker.getSelectedItem().toString();
                    unitOrganisasi = spinnerUnitOrganisasi.getSelectedItem().toString();
                    keterangan = etKeterangan.getEditText().getText().toString();
                    realisasiKeuangan = Long.valueOf(etRealisasiKeuDana.getEditText().getText().toString());
                    progresFisik = Long.valueOf(etProgresFisik.getEditText().getText().toString());

                    getUnitOrganisasiSelected();

                }
            }
        });
    }

    private void getSatkerList() {
        firebaseFirestore.collection("Satker")
                .orderBy("nmsatker", Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                            @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {

                            List<String> satkers = new ArrayList<>();
                            satkers.add("Pilih Satker");

                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
//                                msatker = documentSnapshot.toObject(Satker.class);
                                String nmsatker = String.valueOf(documentSnapshot.get("nmsatker"));
                                satkers.add(nmsatker);
                            }

                            ArrayAdapter<String> satkerAdapter = new ArrayAdapter<String>(UploadLaporanActivity.this,
                                    android.R.layout.simple_spinner_item, satkers);
                            satkerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                            spinnerSatker.setAdapter(satkerAdapter);
                        }
                    }
                });
    }

    private void getUnitOrganisasiList() {
        firebaseFirestore.collection("unitOrganisasi")
                .orderBy("nmUnit", Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                            @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {

                            List<String> unitOrganisasis = new ArrayList<>();
                            unitOrganisasis.add("Pilih Unit Organisasi");

                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
//                                msatker = documentSnapshot.toObject(Satker.class);
                                String nmUnit = String.valueOf(documentSnapshot.get("nmUnit"));
                                unitOrganisasis.add(nmUnit);
                            }

                            ArrayAdapter<String> unitorganisasiAdapter = new ArrayAdapter<String>(
                                    UploadLaporanActivity.this,
                                    android.R.layout.simple_spinner_item, unitOrganisasis);
                            unitorganisasiAdapter
                                    .setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                            spinnerUnitOrganisasi.setAdapter(unitorganisasiAdapter);
                        }
                    }
                });
    }

    private void getUnitOrganisasiSelected(){
        firebaseFirestore.collection("unitOrganisasi")
                .whereEqualTo("nmUnit",unitOrganisasi)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                        if (!task.isSuccessful()){
                            return;
                        }
                        else if (task.getResult().isEmpty()){
                            return;
                        }
                        else {
                            for (DocumentSnapshot doc: task.getResult().getDocuments()){
                                UnitOrganisasi organisasi = doc.toObject(UnitOrganisasi.class);
                                totalPagu = organisasi.getTotalPagu();
                            }
                            simpanlaporan();
                        }
                    }
                });
    }

    private void simpanlaporan() {
        Date date = new Date();
        Timestamp createdAt = new Timestamp(date);

        Laporan laporan = new Laporan(satker, unitOrganisasi, realisasiKeuangan,
                progresFisik, keterangan, createdAt, totalPagu);

        firebaseFirestore.collection("laporan")
                .add(laporan)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        resetField();
                        Toast.makeText(UploadLaporanActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        resetField();
                        Toast.makeText(UploadLaporanActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void resetField() {
        spinnerSatker.setSelection(0);
        spinnerUnitOrganisasi.setSelection(0);

        etKeterangan.getEditText().setText("");
        etKeterangan.setErrorEnabled(false);

        etRealisasiKeuDana.getEditText().setText("");
        etRealisasiKeuDana.setErrorEnabled(false);

        etProgresFisik.getEditText().setText("");
        etProgresFisik.setErrorEnabled(false);
    }

    private void showAlertWhenBackPressed() {
        final Builder alertDialogBuilder = new Builder(this);
        alertDialogBuilder.setMessage("Are you sure to leave?");
        alertDialogBuilder.setPositiveButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alertDialogBuilder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        showAlertWhenBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
