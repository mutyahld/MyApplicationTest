package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 09/02/2019.
 */

public class Satker {
    private String name;
    private String id;

    public Satker() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Satker(String name, String id) {
        this.name = name;
        this.id = id;
    }
}
