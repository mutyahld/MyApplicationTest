package com.example.muthiahalida.myapplicationtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.example.muthiahalida.myapplicationtest.model.InformasiUmum;
import com.example.muthiahalida.myapplicationtest.utils.StringUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.biubiubiu.justifytext.library.JustifyTextView;

public class LatarBelakangActivity extends AppCompatActivity {

    FirebaseFirestore firestore;
    @BindView(R.id.tv_latarbelakang)
    JustifyTextView tvLatarbelakang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latarbelakang_activity);
        ButterKnife.bind(this);
        firestore = FirebaseFirestore.getInstance();
        getlatarBelakang();
    }

    //getlatarBelakang adalah variabel yang kita bikin sendiri
    private void getlatarBelakang() {
        firestore.collection("informasiUmum").document("latarBelakang")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        DocumentSnapshot documentSnapshot = task.getResult();
                        InformasiUmum informasi = documentSnapshot.toObject(InformasiUmum.class);
                        tvLatarbelakang.setText(StringUtil.convertHTMLcode(informasi.getIsi()));
                    }
                });
    }
}