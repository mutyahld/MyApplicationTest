package com.example.muthiahalida.myapplicationtest;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity.Header;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.muthiahalida.myapplicationtest.model.News;
import com.example.muthiahalida.myapplicationtest.model.User;
import com.example.muthiahalida.myapplicationtest.worker.NotificationWorker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query.Direction;
import com.google.firebase.firestore.QuerySnapshot;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nullable;

public class MenuActivity extends AppCompatActivity implements OnNavigationItemSelectedListener {

    @BindView(R.id.absensi_container)
    RelativeLayout absensiContainer;

    Button btninformasiumum;

    Button btnlaporan;

    Button btnformatraker;

    Button btnabsensi;

    Button btnupload;

    Button btnindeksberita;

    Header header;

    @BindView(R.id.laporan_container)
    RelativeLayout laporanContainer;

    @BindView(R.id.raker_container)
    RelativeLayout rakerContainer;

    TextView tvFullName;

    CircleImageView ivGambar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.NavigationV)
    NavigationView NavigationV;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.cirlce)
    LinearLayout llCirlce;

    ProgressBar progressBar;

    @BindView(R.id.upload_container)
    RelativeLayout uploadContainer;

    ViewPager vpNews;

    private ActionBarDrawerToggle toggle;

    private News mnews;

    private String id;

    public String fullName;

    public String gambar;

    private String accessCode;

    FirebaseAuth mAuth;

    FirebaseFirestore firebaseFirestore;

    private List<ImageView> circles;

    private List<Fragment> fragmentList;

    private List<News> newsList;

    private GeneralPagerAdapter pagerAdapter;

    int newsPosition = 0;

    private User mUser;

    private WorkManager workManager;

    private OneTimeWorkRequest requestRaker;

    private OneTimeWorkRequest requestLaporan;

    private AtomicBoolean isFirstListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        vpNews = findViewById(R.id.vp_news);
        btnlaporan = findViewById(R.id.btnlaporan);

        View header = NavigationV.getHeaderView(0);
        tvFullName = (TextView) header.findViewById(R.id.textview_name);
        ivGambar = (CircleImageView) header.findViewById(R.id.iv_gambar);
        progressBar = header.findViewById(R.id.progress_bar_navigationV);

        workManager = WorkManager.getInstance();

        requestLaporan = new OneTimeWorkRequest
                .Builder(NotificationWorker.class)
                .setInputData(setInputData("notif", "Ada Laporan Baru"))
                .build();

        requestRaker = new OneTimeWorkRequest
                .Builder(NotificationWorker.class)
                .setInputData(setInputData("notif", "Ada Raker Baru"))
                .build();

        btnlaporan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, LaporanActivity.class);
                startActivity(intent);
            }
        });

        btninformasiumum = findViewById(R.id.btninformasiumum);

        btninformasiumum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, InformasiUmumActivity.class);
                startActivity(intent);
            }
        });

        btnformatraker = findViewById(R.id.btnformatraker);

        btnformatraker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, FormatRakerActivity.class);
                startActivity(intent);
            }
        });

        btnabsensi = findViewById(R.id.btnabsensi);

        btnabsensi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, AbsensiActivity.class);
                startActivity(intent);
            }
        });

        btnupload = findViewById(R.id.btnupload);

        btnupload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, UploadActivity.class);
                startActivity(intent);
            }
        });

        btnindeksberita = findViewById(R.id.btnindeksberita);

        btnindeksberita.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, IndeksBeritaActivity.class);
                startActivity(intent);
            }
        });

        setNewsData();

        NavigationV.setNavigationItemSelectedListener(this);

        vpNews.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                applyViewPagerCurrentPositon(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setNavigationV();
    }

    private void setupViewPager() {
        fragmentList = new ArrayList<>();
        for (News news : newsList) {
            fragmentList.add(NewsFagment.start(news));
            pagerAdapter = new GeneralPagerAdapter(getSupportFragmentManager(), fragmentList);
            vpNews.setAdapter(pagerAdapter);
            setupCircleIndicatorNews();
        }
    }

    private void setupCircleIndicatorNews() {
        circles = new ArrayList<>();
        llCirlce.removeAllViews();
        if (fragmentList.size() > 0) {
            for (Fragment fragment : fragmentList) {
                ImageView imgCircles = new ImageView(this);
                imgCircles.setImageDrawable(getResources().getDrawable(R.drawable.pasifcircle));
                imgCircles.setPadding(0, 0, 16, 0);
                circles.add(imgCircles);
                llCirlce.addView(imgCircles);
            }
            circles.get(0).setImageDrawable(getResources().getDrawable(R.drawable.circle));
        }
    }

    private void applyViewPagerCurrentPositon(int newsPosition) {
        if (vpNews != null) {
            vpNews.setCurrentItem(newsPosition, true);
            for (ImageView i : circles) {
                i.setImageDrawable(getResources().getDrawable(R.drawable.pasifcircle));
            }
            circles.get(newsPosition).setImageDrawable(getResources().getDrawable(R.drawable.circle));
        }
    }

    private void setNewsData() {
        firebaseFirestore.collection("news")
                .orderBy("createdTime", Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                            @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {
                            newsList = new ArrayList<>();
                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                mnews = documentSnapshot.toObject(News.class);
                                newsList.add(mnews);
                            }
                            setupViewPager();
                            applyViewPagerCurrentPositon(newsPosition);

                        }
                    }
                });

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.Open_Drawer, R.string.Close_Drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getRakerChangesListener();
        getLaporanChangesListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        drawerLayout.removeDrawerListener(toggle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.menu_change_profile:
                toEditProfile();
                break;
            case R.id.menu_logout:
                mAuth.signOut();
                Intent login = new Intent(MenuActivity.this, LoginActivity.class);
                startActivity(login);
                finish();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_notification) {
            Intent intent = new Intent(MenuActivity.this, NotificationActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNavigationV() {
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        id = firebaseUser.getUid();
        progressBar.setVisibility(View.VISIBLE);
        firebaseFirestore.collection("User")
                .document(id)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                            @Nullable FirebaseFirestoreException e) {

                        mUser = documentSnapshot.toObject(User.class);

                        fullName = String.valueOf(documentSnapshot.get("fullName"));
                        gambar = String.valueOf(documentSnapshot.get("uploadPhoto"));
                        accessCode = String.valueOf(documentSnapshot.get("accessCode"));
                        tvFullName.setText(fullName);
                        Glide.with(MenuActivity.this)
                                .load(gambar)
                                .apply(new RequestOptions()
                                        .placeholder(R.drawable.ic_akun)
                                        .error(R.drawable.ic_akun))
                                .into(ivGambar);
                        progressBar.setVisibility(View.GONE);
                        checkUserStatus();
                    }
                });
    }

    private void checkUserStatus() {
        if (accessCode.equalsIgnoreCase("S001")) {
            onUserIsSatker();
        } else {
            onUserIsPejabat();
        }
    }

    private void onUserIsSatker() {
        rakerContainer.setVisibility(View.GONE);
        laporanContainer.setVisibility(View.GONE);
        absensiContainer.setVisibility(View.GONE);
    }

    private void onUserIsPejabat() {
        uploadContainer.setVisibility(View.GONE);
    }

    private void toEditProfile() {
        Intent intent = new Intent(this, EditProfileActivity.class);
        intent.putExtra("CurrentUser", mUser);
        startActivity(intent);
    }

    private void getRakerChangesListener() {
        final AtomicBoolean isFirstListener = new AtomicBoolean(true);
        firebaseFirestore.collection("Raker")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable final QuerySnapshot queryDocumentSnapshots,
                            @Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            return;
                        }

                        if (isFirstListener.get()) {
                            isFirstListener.set(false);
                            return;
                        } else {
                            for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                                switch (dc.getType()) {
                                    case ADDED:
                                        workManager.enqueue(requestRaker);
                                        break;
                                    case MODIFIED:
                                        workManager.enqueue(requestRaker);
                                        break;
                                }
                            }
                        }
                    }
                });
    }

    private void getLaporanChangesListener() {
        isFirstListener = new AtomicBoolean(true);
        firebaseFirestore.collection("laporan")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable final QuerySnapshot queryDocumentSnapshots,
                            @Nullable final FirebaseFirestoreException e) {
                        if (e != null) {
                            return;
                        }

                        if (isFirstListener.get()) {
                            isFirstListener.set(false);
                            return;
                        } else {
                            for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()) {
                                switch (documentChange.getType()) {
                                    case ADDED:
                                        workManager.enqueue(requestLaporan);
                                        break;
                                    case MODIFIED:
                                        workManager.enqueue(requestLaporan);
                                        break;
                                }
                            }
                        }
                    }
                });
    }

    private Data setInputData(String key, String value) {
        return new Data.Builder()
                .putString(key, value)
                .build();
    }
}