package com.example.muthiahalida.myapplicationtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.muthiahalida.myapplicationtest.model.InformasiUmum;
import com.example.muthiahalida.myapplicationtest.utils.StringUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FaqActivity extends AppCompatActivity {

    FirebaseFirestore firestore;
    @BindView(R.id.tv_hubungikami)
    TextView tvHubungikami;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);
        firestore=FirebaseFirestore.getInstance();
        getHubungiKami();
    }

    private void getHubungiKami(){
        firestore.collection("informasiUmum").document("hubungiKami")
                .get()
                //Document Sanpshot mendapatkan Task
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        DocumentSnapshot documentSnapshot = task.getResult();
                        InformasiUmum informasi = documentSnapshot.toObject(InformasiUmum.class);
                        tvHubungikami.setText(StringUtil.convertHTMLcode(informasi.getIsi()));
                    }
                });
    }
}
