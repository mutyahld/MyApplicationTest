package com.example.muthiahalida.myapplicationtest.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.example.muthiahalida.myapplicationtest.Adapter.CommentAdapter.CommentViewHolder;
import com.example.muthiahalida.myapplicationtest.R;
import com.example.muthiahalida.myapplicationtest.model.Comment;
import java.util.List;

/**
 * Created by Muthia Halida on 22/01/2019.
 */

public class CommentAdapter extends Adapter<CommentViewHolder> {

    private Context context;

    private List<Comment> commentList;

    private OnDeleteCommentClickListener mOnDeleteCommentClickListener;

    public CommentAdapter(Context context, List<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
    }

    public void setOnDeleteCommentClickListener(
            final OnDeleteCommentClickListener onDeleteCommentClickListener) {
        mOnDeleteCommentClickListener = onDeleteCommentClickListener;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CommentViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_bottom_sheet_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        holder.bind(getCommentList().get(position));
    }

    @Override
    public int getItemCount() {
        return getCommentList().size();
    }


    public class CommentViewHolder extends ViewHolder {

        @BindView(R.id.iv_comment)
        ImageView ivComment;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_comment)
        TextView tvComment;

        @BindView(R.id.btn_delete)
        ImageView btnDelete;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Comment data) {
            Glide.with(context).load(data.getPhoto()).into(ivComment);
            tvName.setText(data.getName());
            tvComment.setText(data.getComment());

            if (data.isFromUser()){
                btnDelete.setVisibility(View.VISIBLE);
            }else {
                btnDelete.setVisibility(View.GONE);
            }

            btnDelete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {
                    mOnDeleteCommentClickListener.onCommentDeleted(data);
                }
            });
        }
    }

    public interface OnDeleteCommentClickListener{
        void onCommentDeleted(Comment comment);
    }
}