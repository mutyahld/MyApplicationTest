package com.example.muthiahalida.myapplicationtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InformasiUmumActivity extends AppCompatActivity {
    Button btnlatarbelakang;
    Button btndasarhukum;
    Button btnfaq;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasiumum);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        btnlatarbelakang = findViewById(R.id.btnlatarbelakang);
        btnlatarbelakang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InformasiUmumActivity.this, LatarBelakangActivity.class);
                startActivity(intent);
            }
        });

        btndasarhukum = findViewById(R.id.btndasarhukum);
        btndasarhukum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InformasiUmumActivity.this, DasarHukumActivity.class);
                startActivity(intent);
            }
        });

        btnfaq = findViewById(R.id.btnfaq);
        btnfaq.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(InformasiUmumActivity.this, FaqActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
          onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
