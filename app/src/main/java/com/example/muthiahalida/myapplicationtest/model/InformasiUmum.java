package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 05/09/2019.
 */

public class InformasiUmum {
    private String dokumen;
    private String isi;

    //dibawah ini addalah constructor, caranya klik kanan, generate, pilih constructor lalu select none
    public InformasiUmum() {
    }

    //dibawah ini getter dan setter
    public String getDokumen() {
        return dokumen;
    }

    public void setDokumen(String dokumen) {
        this.dokumen = dokumen;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
