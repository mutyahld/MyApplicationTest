package com.example.muthiahalida.myapplicationtest.model;

import com.google.firebase.Timestamp;

/**
 * Created by Muthia Halida on 09/02/2019.
 */

public class Raker {
    private String nmsatker;
    private String nmUnit;
    private long belanjaPegawai;
    private long belanjaModal;
    private long belanjaBarang;
    private long progressFisik;
    private long totalRealisasi;
    private Timestamp createdAt;
    private long paguBelanjaBarang;
    private long paguBelanjaModal;
    private long paguBelanjaPegawai;
    private long progressKeuangan;
    private long totalPagu;

    public Raker() {}

    public Raker(String nmsatker, String nmUnit, long belanjaPegawai, long belanjaModal, long belanjaBarang, long progressFisik) {
        this.nmsatker = nmsatker;
        this.nmUnit = nmUnit;
        this.belanjaPegawai = belanjaPegawai;
        this.belanjaModal = belanjaModal;
        this.belanjaBarang = belanjaBarang;
        this.progressFisik = progressFisik;
    }

    public Raker(final String nmsatker, final String nmUnit, final long belanjaPegawai, final long belanjaModal,
            final long belanjaBarang,
            final long progressFisik, final long totalRealisasi, final Timestamp createdAt,
            final long paguBelanjaBarang, final long paguBelanjaModal,
            final long paguBelanjaPegawai, final long progressKeuangan, final long totalPagu) {
        this.nmsatker = nmsatker;
        this.nmUnit = nmUnit;
        this.belanjaPegawai = belanjaPegawai;
        this.belanjaModal = belanjaModal;
        this.belanjaBarang = belanjaBarang;
        this.progressFisik = progressFisik;
        this.totalRealisasi = totalRealisasi;
        this.createdAt = createdAt;
        this.paguBelanjaBarang = paguBelanjaBarang;
        this.paguBelanjaModal = paguBelanjaModal;
        this.paguBelanjaPegawai = paguBelanjaPegawai;
        this.progressKeuangan = progressKeuangan;
        this.totalPagu = totalPagu;
    }

    public String getNmsatker() {
        return nmsatker;
    }

    public void setNmsatker(String nmsatker) {
        this.nmsatker = nmsatker;
    }

    public String getNmUnit() {
        return nmUnit;
    }

    public void setNmUnit(String nmUnit) {
        this.nmUnit = nmUnit;
    }

    public long getBelanjaPegawai() {
        return belanjaPegawai;
    }

    public void setBelanjaPegawai(long belanjaPegawai) {
        this.belanjaPegawai = belanjaPegawai;
    }

    public long getBelanjaModal() {
        return belanjaModal;
    }

    public void setBelanjaModal(long belanjaModal) {
        this.belanjaModal = belanjaModal;
    }

    public long getBelanjaBarang() {
        return belanjaBarang;
    }

    public void setBelanjaBarang(long belanjaBarang) {
        this.belanjaBarang = belanjaBarang;
    }

    public long getProgressFisik() {
        return progressFisik;
    }

    public void setProgressFisik(long progressFisik) {
        this.progressFisik = progressFisik;
    }

    public long getTotalRealisasi() {
        return totalRealisasi;
    }

    public void setTotalRealisasi(final long totalRealisasi) {
        this.totalRealisasi = totalRealisasi;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public long getPaguBelanjaBarang() {
        return paguBelanjaBarang;
    }

    public void setPaguBelanjaBarang(final long paguBelanjaBarang) {
        this.paguBelanjaBarang = paguBelanjaBarang;
    }

    public long getPaguBelanjaModal() {
        return paguBelanjaModal;
    }

    public void setPaguBelanjaModal(final long paguBelanjaModal) {
        this.paguBelanjaModal = paguBelanjaModal;
    }

    public long getPaguBelanjaPegawai() {
        return paguBelanjaPegawai;
    }

    public void setPaguBelanjaPegawai(final long paguBelanjaPegawai) {
        this.paguBelanjaPegawai = paguBelanjaPegawai;
    }

    public long getProgressKeuangan() {
        return progressKeuangan;
    }

    public void setProgressKeuangan(final long progressKeuangan) {
        this.progressKeuangan = progressKeuangan;
    }

    public long getTotalPagu() {
        return totalPagu;
    }

    public void setTotalPagu(final long totalPagu) {
        this.totalPagu = totalPagu;
    }
}
