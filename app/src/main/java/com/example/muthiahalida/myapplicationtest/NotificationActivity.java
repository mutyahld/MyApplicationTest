package com.example.muthiahalida.myapplicationtest;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.muthiahalida.myapplicationtest.Adapter.NotificationAdapter;
import com.example.muthiahalida.myapplicationtest.model.Notification;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Query.Direction;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Unbinder unbinder;

    @BindView(R.id.rv_notification)
    RecyclerView rvNotification;

    @BindView(R.id.pbNotification)
    ProgressBar pbNotification;

    private List<Notification> notifications;

    private NotificationAdapter adapter;

    private FirebaseFirestore firebaseFirestore;

    private FirebaseUser firebaseUser;

    private String notification;

    private String currentTime;

    SharedPreferences preference;

    Editor editor;

    public static final String MY_PREFERENCES = "MyPreferences";

    private List<Notification> notificationList;

    private List<Notification> allNotifications;

    private String json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pemberitahuan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        notificationList = new ArrayList<>();

        if (VERSION.SDK_INT >= VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        if (getIntent().getExtras() != null) {
            String content = getIntent().getStringExtra("content");
            String time = getIntent().getStringExtra("time");
            long specifiedTime = getIntent().getLongExtra("timestamp", 0);
            Notification notification = new Notification(content, time, specifiedTime);
            saveNotification(notification);
            getNotificationList();
        } else {
            getNotificationList();
        }
    }

    private void saveNotification(Notification notification) {
        firebaseFirestore.collection("Notification")
                .add(notification);
    }

    private void getNotificationList() {
        firebaseFirestore.collection("Notification")
                .orderBy("specifiedTime", Direction.DESCENDING)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {
                            for (DocumentSnapshot doc : queryDocumentSnapshots) {
                                Notification notification = doc.toObject(Notification.class);
                                notificationList.add(notification);
                            }

                            setNotificationList(notificationList);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    private void setNotificationList(List<Notification> notifications) {
        adapter = new NotificationAdapter(this, notifications);
        rvNotification.setLayoutManager(new LinearLayoutManager(this));
        rvNotification.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvNotification.setAdapter(adapter);
        pbNotification.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Intent intent = new Intent(NotificationActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
