package com.example.muthiahalida.myapplicationtest.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.muthiahalida.myapplicationtest.R;
import com.example.muthiahalida.myapplicationtest.model.IndeksBerita;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Muthia Halida on 22/01/2019.
 */

public class IndeksBeritaAdapter extends RecyclerView.Adapter<IndeksBeritaAdapter.IndeksBeritaViewHolder> {

    private Context context;
    private List<IndeksBerita> indeksBeritaList;
    private OnIndeksBeritaClickListener onIndeksBeritaClickListener;

    public IndeksBeritaAdapter(Context context, List<IndeksBerita> indeksBeritaList) {
        this.context = context;
        this.indeksBeritaList = indeksBeritaList;
    }

    public OnIndeksBeritaClickListener getOnIndeksBeritaClickListener() {
        return onIndeksBeritaClickListener;
    }

    public void setOnIndeksBeritaClickListener(OnIndeksBeritaClickListener onIndeksBeritaClickListener) {
        this.onIndeksBeritaClickListener = onIndeksBeritaClickListener;
    }

    public List<IndeksBerita> getIndeksBeritaList() {
        return indeksBeritaList;
    }

    @NonNull
    @Override
    public IndeksBeritaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new IndeksBeritaViewHolder(LayoutInflater.from(context).inflate(R.layout.item_indeks_berita, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull IndeksBeritaViewHolder holder, int position) {
        holder.bind(getIndeksBeritaList().get(position));
    }

    @Override
    public int getItemCount() {
        return getIndeksBeritaList().size();
    }

    public class IndeksBeritaViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_tanggal)
        TextView tvTanggal;
        @BindView(R.id.tv_judul)
        TextView tvJudul;
        @BindView(R.id.tv_link)
        TextView tvLink;

        public IndeksBeritaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(IndeksBerita data) {
            tvTanggal.setText(data.getTanggalUpload());
            tvJudul.setText(data.getJudul());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getOnIndeksBeritaClickListener().onIndeksBeritaClicked(getIndeksBeritaList().get(getAdapterPosition()));
                }
            });
        }
    }


    public interface OnIndeksBeritaClickListener {
        void onIndeksBeritaClicked(IndeksBerita indeksBerita);
    }
}
