package com.example.muthiahalida.myapplicationtest.utils;

import android.content.Context;

public class MeasureUtil {

    public static int pxToDp(Context context, int dps){
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;
    }

}
