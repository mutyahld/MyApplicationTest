package com.example.muthiahalida.myapplicationtest;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.example.muthiahalida.myapplicationtest.model.User;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.storage.UploadTask.TaskSnapshot;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.btnEditPhoto)
    CircleImageView btnEditPhoto;

    @BindView(R.id.btnEditProfile)
    Button btnEditProfile;

    @BindView(R.id.et_name)
    TextInputLayout etName;

    @BindView(R.id.et_phone_number)
    TextInputLayout etPhoneNumber;

    @BindView(R.id.et_status)
    TextView etStatus;

    @BindView(R.id.imgUser)
    CircleImageView imgUser;

    @BindView(R.id.pbEditProfile)
    ProgressBar pbEditProfile;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_registrasi_full_name_Ka_satker)
    TextInputLayout etRegistrasiFullNameKaSatker;

    @BindView(R.id.et_phone_number_ka_satker)
    TextInputLayout etPhoneNumberKaSatker;

    private String uploadPhoto;

    String photoUrl;

    private FirebaseFirestore firebaseFirestore;

    private FirebaseUser firebaseUser;

    private FirebaseStorage firebaseStorage;

    private User currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();

        currentUser = getIntent().getParcelableExtra("CurrentUser");

        Glide.with(this).load(currentUser.getUploadPhoto()).into(imgUser);
        etName.getEditText().setText(currentUser.getFullName());
        etPhoneNumber.getEditText().setText(currentUser.getPhoneNumber());
        etRegistrasiFullNameKaSatker.getEditText().setText(currentUser.getfullNameKaSatker());
        etPhoneNumberKaSatker.getEditText().setText(currentUser.getphoneNumberKaSatker());

        String accessCode = currentUser.getAccessCode();
        if (accessCode.startsWith("S")) {
            etStatus.setText("Satker");
            etRegistrasiFullNameKaSatker.setVisibility(View.VISIBLE);
            etPhoneNumberKaSatker.setVisibility(View.VISIBLE);
        } else {
            etStatus.setText("Pejabat");
        }

        btnEditPhoto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                ImagePicker.create(EditProfileActivity.this)
                        .returnMode(
                                ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                        .includeVideo(true) // Show video on image picker
                        .single() // single mode
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .start(); // start image picker activity with request code
            }
        });

        btnEditProfile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                pbEditProfile.setVisibility(View.VISIBLE);
                saveImageUser();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // or get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);
            String imagePath = image.getPath();
            uploadPhoto = imagePath;
            Glide.with(this).load(imagePath).into(imgUser);
        }
    }

    private void saveImageUser() {
        if (uploadPhoto == null || uploadPhoto.isEmpty()) {
            photoUrl = currentUser.getUploadPhoto();
            editProfile();
        } else {
            Uri photoFile = Uri.fromFile(new File(uploadPhoto));
            StorageReference reference = firebaseStorage.getReference();
            final StorageReference photoUser = reference.child("PhotoUser")
                    .child(photoFile.getLastPathSegment());
            UploadTask uploadTask = photoUser.putFile(photoFile);
            uploadTask.continueWithTask(new Continuation<TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        Toast.makeText(EditProfileActivity.this, "Error " + task.getException().getMessage(),
                                Toast.LENGTH_SHORT).show();
                        pbEditProfile.setVisibility(View.GONE);
                    }
                    return photoUser.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        photoUrl = task.getResult().toString();
                        editProfile();
                    }
                }
            });
        }

    }

    private void editProfile() {
        String fullName = etName.getEditText().getText().toString().trim();
        String phoneNumber = etPhoneNumber.getEditText().getText().toString().trim();
        String fullNameKaSatker = etRegistrasiFullNameKaSatker.getEditText().getText().toString().trim();
        String phoneNumberKaSatker = etPhoneNumberKaSatker.getEditText().getText().toString().trim();


        User newUser = new User(currentUser.getId(), currentUser.getEmail(), fullName, currentUser.getAccessCode(), phoneNumber, photoUrl, fullNameKaSatker, phoneNumberKaSatker
                );
        firebaseFirestore.collection("User")
                .document(currentUser.getId())
                .set(newUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        pbEditProfile.setVisibility(View.GONE);
                        Toast.makeText(EditProfileActivity.this, "Berhasil mengubah data",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        pbEditProfile.setVisibility(View.GONE);
                        Toast.makeText(EditProfileActivity.this, "Error " + e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
