package com.example.muthiahalida.myapplicationtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

/**
 * Created by Muthia Halida on 09/03/2019.
 */

public class Notification implements Parcelable {
    private String notification;
    private String currentTime;
    private long specifiedTime;

    public Notification() {
    }

    public Notification(String notification, String currentTime, long specifiedTime) {
        this.notification = notification;
        this.currentTime = currentTime;
        this.specifiedTime = specifiedTime;
    }

    public Notification(String notification, String currentTime) {
        this.notification = notification;
        this.currentTime = currentTime;
    }

    protected Notification(Parcel in) {
        notification = in.readString();
        currentTime = in.readString();
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {return new Notification[size]; }
    };

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {this.currentTime = currentTime; }

    public long getSpecifiedTime() {
        return specifiedTime;
    }

    public void setSpecifiedTime(long specifiedTime) {
        this.specifiedTime = specifiedTime;
    }

    @Override
    public int describeContents() {return 0;}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(notification);
        dest.writeString(currentTime);
    }
}
