package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 27/03/2019.
 */

public class RealisasiKeuangan {
    private long realisasiBiayaJan;
    private long realisasibiayaFeb;
    private long realisasibiayaMar;
    private long realisasibiayaApr;
    private long realisasibiayaMei;
    private long realisasibiayaJun;
    private long realisasibiayaJul;
    private long realisasibiayaAgu;
    private long realisasibiayaSep;
    private long realisasibiayaOkt;
    private long realisasibiayaNov;
    private long realisasibiayaDes;

    public RealisasiKeuangan() {}

    public RealisasiKeuangan(long realisasiBiayaJan, long realisasibiayaFeb, long realisasibiayaMar, long realisasibiayaApr, long realisasibiayaMei, long realisasibiayaJun, long realisasibiayaJul, long realisasibiayaAgu, long realisasibiayaSep, long realisasibiayaOkt, long realisasibiayaNov, long realisasibiayaDes) {
        this.realisasiBiayaJan = realisasiBiayaJan;
        this.realisasibiayaFeb = realisasibiayaFeb;
        this.realisasibiayaMar = realisasibiayaMar;
        this.realisasibiayaApr = realisasibiayaApr;
        this.realisasibiayaMei = realisasibiayaMei;
        this.realisasibiayaJun = realisasibiayaJun;
        this.realisasibiayaJul = realisasibiayaJul;
        this.realisasibiayaAgu = realisasibiayaAgu;
        this.realisasibiayaSep = realisasibiayaSep;
        this.realisasibiayaOkt = realisasibiayaOkt;
        this.realisasibiayaNov = realisasibiayaNov;
        this.realisasibiayaDes = realisasibiayaDes;
    }

    public long getRealisasiBiayaJan() {
        return realisasiBiayaJan;
    }

    public void setRealisasiBiayaJan(long realisasiBiayaJan) {
        this.realisasiBiayaJan = realisasiBiayaJan;
    }

    public long getRealisasibiayaFeb() {
        return realisasibiayaFeb;
    }

    public void setRealisasibiayaFeb(long realisasibiayaFeb) {
        this.realisasibiayaFeb = realisasibiayaFeb;
    }

    public long getRealisasibiayaMar() {
        return realisasibiayaMar;
    }

    public void setRealisasibiayaMar(long realisasibiayaMar) {
        this.realisasibiayaMar = realisasibiayaMar;
    }

    public long getRealisasibiayaApr() {
        return realisasibiayaApr;
    }

    public void setRealisasibiayaApr(long realisasibiayaApr) {
        this.realisasibiayaApr = realisasibiayaApr;
    }

    public long getRealisasibiayaMei() {
        return realisasibiayaMei;
    }

    public void setRealisasibiayaMei(long realisasibiayaMei) {
        this.realisasibiayaMei = realisasibiayaMei;
    }

    public long getRealisasibiayaJun() {
        return realisasibiayaJun;
    }

    public void setRealisasibiayaJun(long realisasibiayaJun) {
        this.realisasibiayaJun = realisasibiayaJun;
    }

    public long getRealisasibiayaJul() {
        return realisasibiayaJul;
    }

    public void setRealisasibiayaJul(long realisasibiayaJul) {
        this.realisasibiayaJul = realisasibiayaJul;
    }

    public long getRealisasibiayaAgu() {
        return realisasibiayaAgu;
    }

    public void setRealisasibiayaAgu(long realisasibiayaAgu) {
        this.realisasibiayaAgu = realisasibiayaAgu;
    }

    public long getRealisasibiayaSep() {
        return realisasibiayaSep;
    }

    public void setRealisasibiayaSep(long realisasibiayaSep) {
        this.realisasibiayaSep = realisasibiayaSep;
    }

    public long getRealisasibiayaOkt() {
        return realisasibiayaOkt;
    }

    public void setRealisasibiayaOkt(long realisasibiayaOkt) {
        this.realisasibiayaOkt = realisasibiayaOkt;
    }

    public long getRealisasibiayaNov() {
        return realisasibiayaNov;
    }

    public void setRealisasibiayaNov(long realisasibiayaNov) {
        this.realisasibiayaNov = realisasibiayaNov;
    }

    public long getRealisasibiayaDes() {
        return realisasibiayaDes;
    }

    public void setRealisasibiayaDes(long realisasibiayaDes) {
        this.realisasibiayaDes = realisasibiayaDes;
    }
}
