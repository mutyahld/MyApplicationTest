package com.example.muthiahalida.myapplicationtest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

/**
 * Created by Muthia Halida on 19/01/2019.
 */
//Parcelable itu datanya kita bawa kmn2
public class Comment implements Parcelable{

    private String idComment;

    private String idUser;

    private String  photo;

    private String name;

    private String comment;

    private Timestamp currentTime;

    private boolean isFromUser;

    public Comment() {
    }

    public Comment(String photo, String name, String comment, Timestamp currentTime) {
        this.photo = photo;
        this.name = name;
        this.comment = comment;
        this.currentTime = currentTime;
    }

    public Comment(String photo, String name, String comment) {
        this.photo = photo;
        this.name = name;
        this.comment = comment;
    }

    public Comment(final String idUser, final String photo, final String name, final String comment,
            final Timestamp currentTime) {
        this.idUser = idUser;
        this.photo = photo;
        this.name = name;
        this.comment = comment;
        this.currentTime = currentTime;
    }

    public Comment(final String idComment, final String idUser, final String photo, final String name,
            final String comment,
            final Timestamp currentTime) {
        this.idComment = idComment;
        this.idUser = idUser;
        this.photo = photo;
        this.name = name;
        this.comment = comment;
        this.currentTime = currentTime;
    }

    public Comment(final String idComment, final String idUser, final String photo, final String name,
            final String comment,
            final Timestamp currentTime, final boolean isFromUser) {
        this.idComment = idComment;
        this.idUser = idUser;
        this.photo = photo;
        this.name = name;
        this.comment = comment;
        this.currentTime = currentTime;
        this.isFromUser = isFromUser;
    }

    protected Comment(Parcel in) {
        photo = in.readString();
        name = in.readString();
        comment = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public String getIdComment() {
        return idComment;
    }

    public void setIdComment(final String idComment) {
        this.idComment = idComment;
    }

    public boolean isFromUser() {
        return isFromUser;
    }

    public void setFromUser(final boolean fromUser) {
        isFromUser = fromUser;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Timestamp currentTime) {
        this.currentTime = currentTime;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(final String idUser) {
        this.idUser = idUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photo);
        dest.writeString(name);
        dest.writeString(comment);
    }
}
