package com.example.muthiahalida.myapplicationtest;

import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.model.Raker;
import com.example.muthiahalida.myapplicationtest.model.Satker;
import com.example.muthiahalida.myapplicationtest.model.UnitOrganisasi;
import com.example.muthiahalida.myapplicationtest.utils.ValidateUtils;
import com.example.muthiahalida.myapplicationtest.worker.NotificationWorker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Nullable;

public class UploadRakerActivity extends AppCompatActivity {

    long paguBelanjaPegawai;

    long paguBelanjaModal;

    long paguBelanjaBarang;

    long totalPagu;

    long progressKeuangan;

    long belanjaPegawai;

    long belanjaModal;

    long belanjaBarang;

    long progressFisik;

    String satker;

    String unitOrganisasi;

    String idUnitOrganisasi;

    @BindView(R.id.spinner_satker)
    AppCompatSpinner spinnerSatker;

    @BindView(R.id.spinner_unit_organisasi)
    AppCompatSpinner spinnerUnitOrganisasi;

    @BindView(R.id.btn_upload_data)
    Button btnUploadData;

    @BindView(R.id.et_belanja_barang)
    TextInputLayout etBelanjaBarang;

    @BindView(R.id.et_belanja_modal)
    TextInputLayout etBelanjaModal;

    @BindView(R.id.et_belanja_pegawai)
    TextInputLayout etBelanjaPegawai;

    @BindView(R.id.et_progress_fisik)
    TextInputLayout etProgressFisik;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private List<Satker> satkers;

    private List<UnitOrganisasi> unitOrganisasis;

    private Satker msatker;

    private FirebaseFirestore firebaseFirestore;

    private TextInputLayout[] textInputLayouts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_raker);
        ButterKnife.bind(this);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        firebaseFirestore = FirebaseFirestore.getInstance();

        textInputLayouts = new TextInputLayout[]{
                etBelanjaBarang, etBelanjaModal, etBelanjaPegawai, etProgressFisik
        };

        getSatkerList();
        getUnitOrganisasiList();

        ValidateUtils.validateInputUser(textInputLayouts, btnUploadData);



        btnUploadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (spinnerUnitOrganisasi.getSelectedItemPosition() == 0
                        && spinnerSatker.getSelectedItemPosition() == 0) {
                    return;
                } else {
                    belanjaPegawai = Long.valueOf(etBelanjaPegawai.getEditText().getText().toString());
                    belanjaModal = Long.valueOf(etBelanjaModal.getEditText().getText().toString());
                    belanjaBarang = Long.valueOf(etBelanjaBarang.getEditText().getText().toString());
                    progressFisik = Long.valueOf(etProgressFisik.getEditText().getText().toString());
                    satker = spinnerSatker.getSelectedItem().toString();
                    unitOrganisasi = spinnerUnitOrganisasi.getSelectedItem().toString();
                    getUnitOrganisasiSelected();
                }
            }
        });

//        workManager.getWorkInfoByIdLiveData(request.getId())
//                .observe(this, new Observer<WorkInfo>() {
//                    @Override
//                    public void onChanged(@android.support.annotation.Nullable final WorkInfo workInfo) {
//                        if (workInfo!= null){
//                            WorkInfo.State state = workInfo.getState();
//                            Log.d("workinfostatus", "status"+state.toString());
//                        }
//                    }
//                });
    }

    private void getSatkerList() {
        firebaseFirestore.collection("Satker")
                .orderBy("nmsatker", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                            @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {
                            List<String> satkers = new ArrayList<>();
                            satkers.add("Pilih Satker");
                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
//                                msatker = documentSnapshot.toObject(Satker.class);
                                String nmsatker = String.valueOf(documentSnapshot.get("nmsatker"));
                                satkers.add(nmsatker);
                            }

                            ArrayAdapter<String> satkerAdapter = new ArrayAdapter<String>(UploadRakerActivity.this,
                                    android.R.layout.simple_spinner_item, satkers);
                            satkerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                            spinnerSatker.setAdapter(satkerAdapter);
                        }
                    }
                });
    }

    private void getUnitOrganisasiList() {
        firebaseFirestore.collection("unitOrganisasi")
                .orderBy("nmUnit", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                            @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {

                            List<String> unitOrganisasis = new ArrayList<>();
                            unitOrganisasis.add("Pilih Unit Organisasi");

                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                String nmUnit = String.valueOf(documentSnapshot.get("nmUnit"));
                                unitOrganisasis.add(nmUnit);
                            }

                            ArrayAdapter<String> unitorganisasiAdapter = new ArrayAdapter<String>(
                                    UploadRakerActivity.this,
                                    android.R.layout.simple_spinner_item, unitOrganisasis);
                            unitorganisasiAdapter
                                    .setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                            spinnerUnitOrganisasi.setAdapter(unitorganisasiAdapter);
                        }
                    }
                });
    }

    private void getUnitOrganisasiSelected(){
        firebaseFirestore.collection("unitOrganisasi")
                .whereEqualTo("nmUnit",unitOrganisasi)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                        if (!task.isSuccessful()){
                            return;
                        }
                        else if (task.getResult().isEmpty()){
                            return;
                        }
                        else {
                            for (DocumentSnapshot doc: task.getResult().getDocuments()){
                                UnitOrganisasi organisasi = doc.toObject(UnitOrganisasi.class);
                                idUnitOrganisasi = organisasi.getId();
                                paguBelanjaBarang = organisasi.getPaguBelanjaBarang();
                                paguBelanjaModal = organisasi.getPaguBelanjaModal();
                                paguBelanjaPegawai = organisasi.getPaguBelanjaPegawai();
                                totalPagu = organisasi.getTotalPagu();
                                simpanraker();
                            }
                        }
                    }
                });
    }

    private void simpanraker() {
        Date date = new Date();
        Timestamp createdAt = new Timestamp(date);
        long totalRealisasi = belanjaBarang+belanjaModal+belanjaPegawai;
        progressKeuangan = (totalRealisasi/totalPagu)*100;

        Raker raker = new Raker(satker, unitOrganisasi, belanjaPegawai, belanjaModal, belanjaBarang, progressFisik,
                totalRealisasi,createdAt,paguBelanjaBarang,paguBelanjaModal,paguBelanjaPegawai,progressKeuangan,totalPagu);

        firebaseFirestore.collection("Raker")
                .document(idUnitOrganisasi)
                .set(raker)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull final Task<Void> task) {
                        resetField();
                        Toast.makeText(UploadRakerActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        resetField();
                        Toast.makeText(UploadRakerActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void resetField() {
        spinnerSatker.setSelection(0);
        spinnerUnitOrganisasi.setSelection(0);

        etBelanjaPegawai.getEditText().setText("");
        etBelanjaPegawai.setErrorEnabled(false);

        etBelanjaBarang.getEditText().setText("");
        etBelanjaBarang.setErrorEnabled(false);

        etBelanjaModal.getEditText().setText("");
        etBelanjaModal.setErrorEnabled(false);

        etProgressFisik.getEditText().setText("");
        etProgressFisik.setErrorEnabled(false);
    }

    private void showAlertWhenBackPressed() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure to leave?");
        alertDialogBuilder.setPositiveButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alertDialogBuilder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        showAlertWhenBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendMessageToServer() {
        String topic = "raker";
        String SENDER_ID = "666970617524";
        int messageId = 0;

        FirebaseMessaging fm = FirebaseMessaging.getInstance();
        fm.send(new RemoteMessage.Builder(SENDER_ID + "666970617524")
                .setMessageId(Integer.toString(messageId))
                .addData("message", "tes")
                .addData("action", "tessss")
                .build());
    }
}