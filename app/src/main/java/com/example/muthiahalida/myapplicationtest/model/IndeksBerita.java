package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 22/01/2019.
 */

public class IndeksBerita {
    private String tanggalUpload;
    private String judul;
    private String file;

    public IndeksBerita(String tanggalUpload, String judul, String file) {
        this.tanggalUpload = tanggalUpload;
        this.judul = judul;
        this.file = file;
    }

    public String getTanggalUpload() {
        return tanggalUpload;
    }

    public void setTanggalUpload(String tanggalUpload) {
        this.tanggalUpload = tanggalUpload;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
