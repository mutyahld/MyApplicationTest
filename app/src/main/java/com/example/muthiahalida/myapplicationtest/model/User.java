package com.example.muthiahalida.myapplicationtest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Muthia Halida on 31/01/2019.
 */

public class User implements Parcelable {

    private String id;

    private String email;

    private String fullName;

    private String accessCode;

    private String phoneNumber;

    private String uploadPhoto;

    private String fullNameKaSatker;

    private String phoneNumberKaSatker;

    public User() {
    }

    public User(String id, String email, String fullName, String accessCode, String phoneNumber, String uploadPhoto, String fullNameKaSatker, String phoneNumberKaSatker) {
        this.id = id;
        this.email = email;
        this.fullName = fullName;
        this.accessCode = accessCode;
        this.phoneNumber = phoneNumber;
        if (uploadPhoto == null){
            uploadPhoto = "";
        }
        this.uploadPhoto = uploadPhoto;
        this.fullNameKaSatker = fullNameKaSatker;
        this.phoneNumberKaSatker = phoneNumberKaSatker;
    }

    protected User(Parcel in) {
        id = in.readString();
        email = in.readString();
        fullName = in.readString();
        accessCode = in.readString();
        phoneNumber = in.readString();
        uploadPhoto = in.readString();
        fullNameKaSatker = in.readString();
        phoneNumberKaSatker = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUploadPhoto() {return uploadPhoto;}

    public void setUploadPhoto(String uploadPhoto) {this.uploadPhoto = uploadPhoto;}


    public String getfullNameKaSatker() {return fullNameKaSatker;}

    public void setfullNameKaSatker(String fullNameKaSatker) {this.fullNameKaSatker = fullNameKaSatker;}

    public String getphoneNumberKaSatker() {return phoneNumberKaSatker;}

    public void setphoneNumberKaSatker(String phoneNumberKaSatker) {this.phoneNumberKaSatker = phoneNumberKaSatker;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(fullName);
        dest.writeString(accessCode);
        dest.writeString(phoneNumber);
        dest.writeString(uploadPhoto);
        dest.writeString(fullNameKaSatker);
        dest.writeString(phoneNumberKaSatker);
    }
}
