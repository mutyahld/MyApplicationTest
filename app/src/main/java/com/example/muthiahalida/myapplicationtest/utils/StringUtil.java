package com.example.muthiahalida.myapplicationtest.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * Created by Muthia Halida on 06/09/2019.
 */

public class StringUtil {
    public static String convertHTMLcode(String htmlCode){
        Spanned html;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            html=Html.fromHtml(htmlCode, Html.FROM_HTML_MODE_COMPACT);
        } else {
            html=Html.fromHtml(htmlCode);
        }

        return html.toString();

    }
}
