package com.example.muthiahalida.myapplicationtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.utils.ValidateUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class LoginActivity extends AppCompatActivity {

    TextInputLayout et_username;

    TextInputLayout et_password;

    Button btn_login;

    @BindView(R.id.pbLogin)
    ProgressBar pbLogin;

    @BindView(R.id.tv_forgort_password)
    TextView tvForgortPassword;

    TextView tv_new_account;

    private FirebaseAuth mAuth;

    private TextInputLayout[] textInputLayouts;

    String Email;

    String Password;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_login);
        tv_new_account = findViewById(R.id.tv_new_account);

        textInputLayouts = new TextInputLayout[]{
                et_username, et_password
        };

        ValidateUtils.validateInputUser(textInputLayouts, btn_login);

        if (isUserLogin()) {
            loginSuccess();
        }

        btn_login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                pbLogin.setVisibility(View.VISIBLE);
                Email = et_username.getEditText().getText().toString();
                Password = et_password.getEditText().getText().toString();
                checkLogin();
            }
        });

        tv_new_account.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewAccount();
            }
        });

        tvForgortPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isUserLogin()) {
            loginSuccess();
        }
    }

    public boolean isUserLogin() {
        boolean isLogin = false;
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        if (firebaseUser != null) {
            isLogin = true;
        }
        return isLogin;
    }

    public void checkLogin() {
        mAuth.signInWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            loginSuccess();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(LoginActivity.this, e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void loginSuccess() {
        pbLogin.setVisibility(View.GONE);
        getFcmToken();
        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    public void createNewAccount() {
        Intent intent = new Intent(LoginActivity.this, RegistrasiActivity.class);
        startActivity(intent);
    }

    private String getFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        token = task.getResult().getToken();
                        Log.d("token fcm ", token);
                    }
                });
        return token;
    }
}