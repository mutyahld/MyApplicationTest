package com.example.muthiahalida.myapplicationtest;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.muthiahalida.myapplicationtest.utils.ValidateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.SimpleTimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    String username;
    String password;
    String address;
    String religion;
    String gender;
    String birthDate;

    @BindView(R.id.et_register_username)
    TextInputLayout etRegisterUsername;

    @BindView(R.id.et_register_password)
    TextInputLayout etRegisterPassword;

    @BindView(R.id.et_register_address)
    TextInputLayout etRegisterAddress;

    @BindView(R.id.spinner_religion)
    AppCompatSpinner spinnerReligion;

    @BindView(R.id.rb_gender_female)
    AppCompatRadioButton rbGenderFemale;

    @BindView(R.id.rb_gender_male)
    AppCompatRadioButton rbGenderMale;

    @BindView(R.id.btn_registration)
    Button btnRegistration;

    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.iv_calender)
    ImageView ivCalender;

    @BindView(R.id.tv_birthdate)
    TextView tvBirthdate;


    private TextInputLayout[] textInputLayouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        textInputLayouts = new TextInputLayout[]{
                etRegisterUsername, etRegisterPassword, etRegisterAddress
        };

        ValidateUtils.validateInputUser(textInputLayouts, btnRegistration);

        setReligion();
        setGenderRadioButton();

        ivCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDatePicker();
            }
        });

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = etRegisterUsername.getEditText().getText().toString();
                password = etRegisterPassword.getEditText().getText().toString();
                address = etRegisterAddress.getEditText().getText().toString();
                religion = spinnerReligion.getSelectedItem().toString();
//
//                        checkLogin();
            }
        });


    }

    private void setReligion() {
        List<String> religions = new ArrayList<>();
        religions.add(new String("Islam"));
        religions.add(new String("Kristen"));
        religions.add(new String("Buddha"));

        ArrayAdapter<String> religionAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, religions);
        religionAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinnerReligion.setAdapter(religionAdapter);

    }

    private void setGenderRadioButton() {
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_gender_female:
                        gender = "female";
                        break;
                    case R.id.rb_gender_male:
                        gender = "male";
                        break;
                }
            }
        });
    }

    private void setDatePicker() {

                Calendar calendar = Calendar.getInstance();
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mmm/yyyy");

                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this);

                datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, dayOfMonth);

                        birthDate = simpleDateFormat.format(newDate.getTime());
                        tvBirthdate.setText(birthDate);
                    }

                });

                datePickerDialog.show();

    }
}
