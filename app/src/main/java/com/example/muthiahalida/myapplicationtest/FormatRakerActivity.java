package com.example.muthiahalida.myapplicationtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.model.Raker;
import com.example.muthiahalida.myapplicationtest.utils.MeasureUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.List;

public class FormatRakerActivity extends AppCompatActivity {

    @BindView(R.id.TableLayout1)
    TableLayout TableLayout1;

    @BindView(R.id.TableRow1)
    TableRow TableRow1;

    @BindView(R.id.TextViewNo)
    TextView TextViewNo;

    FirebaseFirestore firebaseFirestore;

    @BindView(R.id.tl_laporan2)
    TableLayout tlLaporan2;

    @BindView(R.id.tl_laporan3)
    TableLayout tlLaporan3;

    @BindView(R.id.tl_laporan4)
    TableLayout tlLaporan4;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tr_laporan2)
    TableRow trLaporan2;

    @BindView(R.id.tr_laporan3)
    TableRow trLaporan3;

    @BindView(R.id.tr_laporan4)
    TableRow trLaporan4;

    @BindView(R.id.tv_judul_data_raker)
    TextView tvJudulDataRaker;

    @BindView(R.id.tv_pagu_anggaran)
    TextView tvPaguAnggaran;

    @BindView(R.id.tv_pagu_belanja_barang)
    TextView tvPaguBelanjaBarang;

    @BindView(R.id.tv_pagu_belanja_modal)
    TextView tvPaguBelanjaModal;

    @BindView(R.id.tv_pagu_belanja_pegawai)
    TextView tvPaguBelanjaPegawai;

    @BindView(R.id.tv_progres_fisik)
    TextView tvProgresFisik;

    @BindView(R.id.tv_Progress)
    TextView tvProgress;

    @BindView(R.id.tv_progress_keuangan)
    TextView tvProgressKeuangan;

    @BindView(R.id.tv_realisasi)
    TextView tvRealisasi;

    @BindView(R.id.tv_realisasi_belanja_barang)
    TextView tvRealisasiBelanjaBarang;

    @BindView(R.id.tv_realisasi_belanja_modal)
    TextView tvRealisasiBelanjaModal;

    @BindView(R.id.tv_realisasi_belanja_pegawai)
    TextView tvRealisasiBelanjaPegawai;

    @BindView(R.id.tv_total_pagu)
    TextView tvTotalPagu;

    @BindView(R.id.tv_total_realisasi)
    TextView tvTotalRealisasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formatraker);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Raker");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        firebaseFirestore = FirebaseFirestore.getInstance();

        getRakerList();
    }

    private void getRakerList() {
        firebaseFirestore.collection("Raker")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {

                            List<Raker> rakerList = new ArrayList<>();

                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                Raker raker = documentSnapshot.toObject(Raker.class);
                                rakerList.add(raker);
                            }

                            setDataTable(rakerList);
                        }
                    }
                });
    }

    private void setDataTable(List<Raker> rakerList) {
        for (int i = 0; i < rakerList.size(); i++) {
            TableRow row = new TableRow(this);

            TextView tvNo = new TextView(this);
            tvNo.setText(String.valueOf(i + 1));
            tvNo.setGravity(Gravity.CENTER);
            tvNo.setBackground(getResources().getDrawable(R.drawable.border5));
            tvNo.setPadding(10, 10, 10, 10);
            row.addView(tvNo);

            TextView tvUnitOrganisasi = new TextView(this);
            tvUnitOrganisasi.setText(rakerList.get(i).getNmUnit());
            tvUnitOrganisasi.setGravity(Gravity.CENTER);
            tvUnitOrganisasi.setBackground(getResources().getDrawable(R.drawable.border5));
            tvUnitOrganisasi.setPadding(10, 10, 10, 10);
            row.addView(tvUnitOrganisasi);

            TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT, 1f);

            TableLayout tabPagu = new TableLayout(this);
            tabPagu.addView(setDataPagu(rakerList, i));
            row.addView(tabPagu);

            TableLayout tabRealisasi = new TableLayout(this);
            tabRealisasi.addView(setDataRealisasi(rakerList, i));
            row.addView(tabRealisasi);

            TableLayout tabProgress = new TableLayout(this);
            tabProgress.addView(setDataProgress(rakerList, i));
            row.addView(tabProgress);

            TableLayout1.addView(row);
        }
    }



    private View setDataPagu(List<Raker> rakerList, int i) {
        TableRow row = new TableRow(this);
        TableRow.LayoutParams params1 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT, 1f);
        row.setLayoutParams(params1);

        TextView tvPaguBelanjaPegawai = new TextView(this);
        tvPaguBelanjaPegawai.setWidth(MeasureUtil.pxToDp(this,150));
        tvPaguBelanjaPegawai.setText(String.valueOf(rakerList.get(i).getPaguBelanjaPegawai()));
        tvPaguBelanjaPegawai.setGravity(Gravity.CENTER);
        tvPaguBelanjaPegawai.setBackground(getResources().getDrawable(R.drawable.border5));
        tvPaguBelanjaPegawai.setPadding(10, 10, 10, 10);
        row.addView(tvPaguBelanjaPegawai);

        TextView tvPaguBelanjaBarang = new TextView(this);
        tvPaguBelanjaBarang.setWidth(MeasureUtil.pxToDp(this,150));
        tvPaguBelanjaBarang.setText(String.valueOf(rakerList.get(i).getPaguBelanjaBarang()));
        tvPaguBelanjaBarang.setGravity(Gravity.CENTER);
        tvPaguBelanjaBarang.setBackground(getResources().getDrawable(R.drawable.border5));
        tvPaguBelanjaBarang.setPadding(10, 10, 10, 10);
        row.addView(tvPaguBelanjaBarang);

        TextView tvPaguBelanjaModal = new TextView(this);
        tvPaguBelanjaModal.setWidth(MeasureUtil.pxToDp(this,150));
        tvPaguBelanjaModal.setText(String.valueOf(rakerList.get(i).getPaguBelanjaModal()));
        tvPaguBelanjaModal.setGravity(Gravity.CENTER);
        tvPaguBelanjaModal.setBackground(getResources().getDrawable(R.drawable.border5));
        tvPaguBelanjaModal.setPadding(10, 10, 10, 10);
        row.addView(tvPaguBelanjaModal);

        TextView totalPagu = new TextView(this);
        totalPagu.setWidth(MeasureUtil.pxToDp(this,150));
        totalPagu.setText(String.valueOf(rakerList.get(i).getTotalPagu()));
        totalPagu.setGravity(Gravity.CENTER);
        totalPagu.setBackground(getResources().getDrawable(R.drawable.border5));
        totalPagu.setPadding(10, 10, 10, 10);
        row.addView(totalPagu);

        return row;
    }

    private View setDataRealisasi(List<Raker> rakerList, int i) {
        TableRow row = new TableRow(this);
        TableRow.LayoutParams params1 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT, 1f);
        row.setLayoutParams(params1);

        TextView tvRealisasiBelanjaPegawai = new TextView(this);
        tvRealisasiBelanjaPegawai.setWidth(MeasureUtil.pxToDp(this,150));
        tvRealisasiBelanjaPegawai.setText(String.valueOf(rakerList.get(i).getBelanjaPegawai()));
        tvRealisasiBelanjaPegawai.setGravity(Gravity.CENTER);
        tvRealisasiBelanjaPegawai.setBackground(getResources().getDrawable(R.drawable.border5));
        tvRealisasiBelanjaPegawai.setPadding(10, 10, 10, 10);
        row.addView(tvRealisasiBelanjaPegawai);

        TextView tvRealisasiBelanjaBarang = new TextView(this);
        tvRealisasiBelanjaBarang.setWidth(MeasureUtil.pxToDp(this,150));
        tvRealisasiBelanjaBarang.setText(String.valueOf(rakerList.get(i).getBelanjaBarang()));
        tvRealisasiBelanjaBarang.setGravity(Gravity.CENTER);
        tvRealisasiBelanjaBarang.setBackground(getResources().getDrawable(R.drawable.border5));
        tvRealisasiBelanjaBarang.setPadding(10, 10, 10, 10);
        row.addView(tvRealisasiBelanjaBarang);

        TextView tvRealisasiBelanjaModal = new TextView(this);
        tvRealisasiBelanjaModal.setWidth(MeasureUtil.pxToDp(this,150));
        tvRealisasiBelanjaModal.setText(String.valueOf(rakerList.get(i).getBelanjaModal()));
        tvRealisasiBelanjaModal.setGravity(Gravity.CENTER);
        tvRealisasiBelanjaModal.setBackground(getResources().getDrawable(R.drawable.border5));
        tvRealisasiBelanjaModal.setPadding(10, 10, 10, 10);
        row.addView(tvRealisasiBelanjaModal);

        TextView totalRealisasi = new TextView(this);
        totalRealisasi.setWidth(MeasureUtil.pxToDp(this,150));
        totalRealisasi.setText(String.valueOf(rakerList.get(i).getTotalRealisasi()));
        totalRealisasi.setGravity(Gravity.CENTER);
        totalRealisasi.setBackground(getResources().getDrawable(R.drawable.border5));
        totalRealisasi.setPadding(10, 10, 10, 10);
        row.addView(totalRealisasi);

        return row;
    }

    private View setDataProgress(List<Raker> rakerList, int i) {
        TableRow row = new TableRow(this);
        TableRow.LayoutParams params1 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT, 1f);
        row.setLayoutParams(params1);

        TextView tvProgressKeuangan = new TextView(this);
        tvProgressKeuangan.setWidth(MeasureUtil.pxToDp(this,150));
        tvProgressKeuangan.setText(String.valueOf(rakerList.get(i).getProgressKeuangan()));
        tvProgressKeuangan.setGravity(Gravity.CENTER);
        tvProgressKeuangan.setBackground(getResources().getDrawable(R.drawable.border5));
        tvProgressKeuangan.setPadding(10, 10, 10, 10);
        row.addView(tvProgressKeuangan);

        TextView tvProgressFisik = new TextView(this);
        tvProgressFisik.setWidth(MeasureUtil.pxToDp(this,150));
        tvProgressFisik.setText(String.valueOf(rakerList.get(i).getProgressFisik()));
        tvProgressFisik.setGravity(Gravity.CENTER);
        tvProgressFisik.setBackground(getResources().getDrawable(R.drawable.border5));
        tvProgressFisik.setPadding(10, 10, 10, 10);
        row.addView(tvProgressFisik);

        return row;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
