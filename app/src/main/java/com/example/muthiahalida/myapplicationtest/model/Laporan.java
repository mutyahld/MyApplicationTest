package com.example.muthiahalida.myapplicationtest.model;

import android.support.annotation.NonNull;
import com.google.firebase.Timestamp;

/**
 * Created by Muthia Halida on 20/03/2019.
 */

public class Laporan implements Comparable<Laporan>{

    private String nmsatker;
    private String nmUnit;
    private long realisasiKeuDana;
    private long progresFisik;
    private String keterangan;
    private Timestamp createdAt;
    private long totalPagu;

    public Laporan() {}

    public Laporan(final String nmsatker, final String nmUnit, final long realisasiKeuDana, final long progresFisik,
            final String keterangan,
            final Timestamp createdAt, final long totalPagu) {
        this.nmsatker = nmsatker;
        this.nmUnit = nmUnit;
        this.realisasiKeuDana = realisasiKeuDana;
        this.progresFisik = progresFisik;
        this.keterangan = keterangan;
        this.createdAt = createdAt;
        this.totalPagu = totalPagu;
    }

    @Override
    public int compareTo(@NonNull final Laporan o) {
        if (getCreatedAt() == null || o.getCreatedAt() == null){
            return 0;
        }
        return getCreatedAt().compareTo(o.getCreatedAt());
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(final String keterangan) {
        this.keterangan = keterangan;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public long getTotalPagu() {
        return totalPagu;
    }

    public void setTotalPagu(final long totalPagu) {
        this.totalPagu = totalPagu;
    }

    public String getNmsatker() {
        return nmsatker;
    }

    public void setNmsatker(String nmsatker) {
        this.nmsatker = nmsatker;
    }

    public String getNmUnit() {
        return nmUnit;
    }

    public void setNmUnit(String nmUnit) {
        this.nmUnit = nmUnit;
    }

    public long getRealisasiKeuDana() {
        return realisasiKeuDana;
    }

    public void setRealisasiKeuDana(long realisasiKeuDana) {this.realisasiKeuDana = realisasiKeuDana;}

    public long getProgresFisik() {
        return progresFisik;
    }

    public void setProgresFisik(long progresFisik) {
        this.progresFisik = progresFisik;
    }
}
