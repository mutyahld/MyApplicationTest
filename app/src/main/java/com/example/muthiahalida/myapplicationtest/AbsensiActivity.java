package com.example.muthiahalida.myapplicationtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.model.Laporan;
import com.example.muthiahalida.myapplicationtest.model.UnitOrganisasi;
import com.example.muthiahalida.myapplicationtest.utils.DateUtils;
import com.example.muthiahalida.myapplicationtest.utils.MeasureUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AbsensiActivity extends AppCompatActivity {

    @BindView(R.id.sv_laporan)
    ScrollView svLaporan;

    @BindView(R.id.tl_laporan)
    TableLayout tlLaporan;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tr_laporan)
    TableRow trLaporan;

    @BindView(R.id.tv_akumulasi_sampai_hari_ini)
    TextView tvAkumulasiSampaiHariIni;

    @BindView(R.id.tv_bulan_ini)
    TextView tvBulanIni;

    @BindView(R.id.tv_hari_ini)
    TextView tvHariIni;

    @BindView(R.id.tv_judul_data_laporan)
    TextView tvJudulDataLaporan;

    @BindView(R.id.tv_jumlah_satker)
    TextView tvJumlahSatker;

    @BindView(R.id.tv_jumlah_satker_pengirim)
    TextView tvJumlahSatkerPengirim;

    @BindView(R.id.tv_no)
    TextView tvNo;

    @BindView(R.id.tv_presentase)
    TextView tvPresentase;

    @BindView(R.id.tvToday)
    TextView tvToday;

    @BindView(R.id.tv_total_satker)
    TextView tvTotalSatker;

    @BindView(R.id.tv_unit_organisasi)
    TextView tvUnitOrganisasi;

    private FirebaseFirestore mFirebaseFirestore;

    int no;

    String nmUnit;

    long totalSatker;

    int todayAbsence;

    int monthAbsence;

    int todayAcumulation;

    long progressFisik;

    private HashMap<String, List<Laporan>> laporanDateMap;

    private HashMap<String, List<Laporan>> laporanOrganisasiMap;

    private HashMap<String, List<Laporan>> laporanSatkerMap;

    private Date currentDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Absensi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFirebaseFirestore = FirebaseFirestore.getInstance();

        currentDay = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        String today = sdf.format(currentDay);
        tvToday.setText(String.format(getString(R.string.label_today), today));

        getAbsence();

    }

    private void getAbsence() {
        mFirebaseFirestore.collection("laporan")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(final QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots == null) {
                            return;
                        } else if (queryDocumentSnapshots.isEmpty()) {
                            return;
                        } else {
                            List<Laporan> laporanList = new ArrayList<>();

                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                Laporan laporan = documentSnapshot.toObject(Laporan.class);
                                laporanList.add(laporan);
                            }

                            laporanBasedOnDate(laporanList);
                        }
                    }
                });
    }

    private void laporanBasedOnDate(List<Laporan> laporanList) {
        Collections.sort(laporanList);
        laporanDateMap = new HashMap<>();
        for (Laporan laporan : laporanList) {
            String month = DateUtils.toSpecifictMonthInCalendar(laporan.getCreatedAt());
            if (!laporanDateMap.containsKey(month)) {
                List<Laporan> laporanPerMonth = new ArrayList<>();
                laporanPerMonth.add(laporan);
                laporanDateMap.put(month, laporanPerMonth);
            } else {
                laporanDateMap.get(month).add(laporan);
            }
        }

        List<String> dateKeys = new ArrayList<>(laporanDateMap.keySet());

        for (String key : dateKeys) {
            List<Laporan> laporans = laporanDateMap.get(key);
            laporanBasedOnOrganisasi(laporans);
        }
    }

    private void laporanBasedOnOrganisasi(List<Laporan> laporanList) {
        laporanOrganisasiMap = new HashMap<>();

        for (Laporan laporan : laporanList) {
            String nmUnit = laporan.getNmUnit();
            if (!laporanOrganisasiMap.containsKey(nmUnit)) {
                List<Laporan> laporanPerSatker = new ArrayList<>();
                laporanPerSatker.add(laporan);
                laporanOrganisasiMap.put(nmUnit, laporanPerSatker);
            } else {
                laporanOrganisasiMap.get(nmUnit).add(laporan);
            }
        }

        List<String> orgKeys = new ArrayList<>(laporanOrganisasiMap.keySet());
        no = 0;
        for (String key : orgKeys) {
            no += 1;
            getUnitOrganisasiSelected(key);
            List<Laporan> laporans = laporanOrganisasiMap.get(key);
            nmUnit = key;
            todayAcumulation = laporans.size();
            progressFisik = laporans.get(laporans.size() - 1).getProgresFisik();
            laporanBasedOnSatker(laporans);
        }
    }

    private void laporanBasedOnSatker(List<Laporan> laporanList) {
        laporanSatkerMap = new HashMap<>();
        for (Laporan laporan : laporanList) {
            String satker = laporan.getNmsatker();
            if (!laporanSatkerMap.containsKey(satker)) {
                List<Laporan> laporanPerSatker = new ArrayList<>();
                laporanPerSatker.add(laporan);
                laporanSatkerMap.put(satker, laporanPerSatker);
            } else {
                laporanSatkerMap.get(satker).add(laporan);
            }
        }

        List<String> satkerKeys = new ArrayList<>(laporanSatkerMap.keySet());

        for (String key : satkerKeys) {
            List<Laporan> laporans = laporanSatkerMap.get(key);
            todayAbsence = getTodayAbsence(laporans);
            monthAbsence = getMonthAbsence(laporans);
        }
        setDataTable(no, nmUnit, totalSatker, todayAbsence, monthAbsence, todayAcumulation, progressFisik);
    }

    private void getUnitOrganisasiSelected(String unitOrganisasi) {
        mFirebaseFirestore.collection("unitOrganisasi")
                .whereEqualTo("nmUnit", unitOrganisasi)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                        if (!task.isSuccessful()) {
                            return;
                        } else if (task.getResult().isEmpty()) {
                            return;
                        } else {
                            for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                                UnitOrganisasi organisasi = doc.toObject(UnitOrganisasi.class);
                                totalSatker = organisasi.getTotalSatker();
                            }
                        }
                    }
                });
    }

    private int getTodayAbsence(List<Laporan> laporanList) {
        String today = DateFormat.format("dd", currentDay).toString();
        List<Laporan> todayList = new ArrayList<>();

        for (Laporan laporan : laporanList) {
            String createdAt = DateFormat.format("dd", laporan.getCreatedAt().toDate()).toString();
            if (createdAt.equals(today)) {
                todayList.add(laporan);
            }
        }

        return todayList.size();
    }

    private int getMonthAbsence(List<Laporan> laporanList) {
        String thisMonth = DateFormat.format("MM", currentDay).toString();
        List<Laporan> monthList = new ArrayList<>();

        for (Laporan laporan : laporanList) {
            String createdAt = DateFormat.format("MM", laporan.getCreatedAt().toDate()).toString();
            if (createdAt.equals(thisMonth)) {
                monthList.add(laporan);
            }
        }

        return monthList.size();
    }

    private void setDataTable(int no, String nmUnit, long totalSatker, int todayAbsence, int monthAbsence,
            int todayAcumulation, long progressFisik) {
        TableRow row = new TableRow(this);

        TextView tvNo = new TextView(this);
        tvNo.setText(String.valueOf(no));
        tvNo.setGravity(Gravity.CENTER);
        tvNo.setBackground(getResources().getDrawable(R.drawable.border5));
        tvNo.setPadding(10, 10, 10, 10);
        row.addView(tvNo);

        TextView tvUnitOrganisasi = new TextView(this);
        tvUnitOrganisasi.setText(nmUnit);
        tvUnitOrganisasi.setGravity(Gravity.CENTER);
        tvUnitOrganisasi.setBackground(getResources().getDrawable(R.drawable.border5));
        tvUnitOrganisasi.setPadding(10, 10, 10, 10);
        row.addView(tvUnitOrganisasi);

        TextView tvJumlahSatker = new TextView(this);
        tvJumlahSatker.setText(String.valueOf(totalSatker));
        tvJumlahSatker.setGravity(Gravity.CENTER);
        tvJumlahSatker.setBackground(getResources().getDrawable(R.drawable.border5));
        tvJumlahSatker.setPadding(10, 10, 10, 10);
        row.addView(tvJumlahSatker);

        TableLayout tabTotalSatker = new TableLayout(this);
        tabTotalSatker.addView(setTotalSatker(todayAbsence, monthAbsence, todayAcumulation, progressFisik));
        row.addView(tabTotalSatker);

        tlLaporan.addView(row);
    }

    private View setTotalSatker(int todayAbsence, int monthAbsence, int todayAcumulation, long progressFisik) {
        TableRow row = new TableRow(this);
        LayoutParams params1 = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        row.setLayoutParams(params1);

        TextView tvTodayAbsence = new TextView(this);
        tvTodayAbsence.setWidth(MeasureUtil.pxToDp(this, 150));
        tvTodayAbsence.setText(String.valueOf(todayAbsence));
        tvTodayAbsence.setGravity(Gravity.CENTER);
        tvTodayAbsence.setBackground(getResources().getDrawable(R.drawable.border5));
        tvTodayAbsence.setPadding(10, 10, 10, 10);
        row.addView(tvTodayAbsence);

        TextView tvMonthAbsence = new TextView(this);
        tvMonthAbsence.setWidth(MeasureUtil.pxToDp(this, 150));
        tvMonthAbsence.setText(String.valueOf(monthAbsence));
        tvMonthAbsence.setGravity(Gravity.CENTER);
        tvMonthAbsence.setBackground(getResources().getDrawable(R.drawable.border5));
        tvMonthAbsence.setPadding(10, 10, 10, 10);
        row.addView(tvMonthAbsence);

        TableLayout tabTotalData = new TableLayout(this);
        tabTotalData.addView(setTotalData(todayAcumulation, progressFisik));
        row.addView(tabTotalData);

        return row;
    }

    private View setTotalData(int todayAcumulation, long progressFisik) {
        TableRow row = new TableRow(this);
        LayoutParams params1 = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        row.setLayoutParams(params1);

        TextView tvTodayAcumulation = new TextView(this);
        tvTodayAcumulation.setWidth(MeasureUtil.pxToDp(this, 150));
        tvTodayAcumulation.setText(String.valueOf(todayAcumulation));
        tvTodayAcumulation.setGravity(Gravity.CENTER);
        tvTodayAcumulation.setBackground(getResources().getDrawable(R.drawable.border5));
        tvTodayAcumulation.setPadding(10, 10, 10, 10);
        row.addView(tvTodayAcumulation);

        TextView tvProgressFisik = new TextView(this);
        tvProgressFisik.setWidth(MeasureUtil.pxToDp(this, 150));
        tvProgressFisik.setText(String.valueOf(progressFisik));
        tvProgressFisik.setGravity(Gravity.CENTER);
        tvProgressFisik.setBackground(getResources().getDrawable(R.drawable.border5));
        tvProgressFisik.setPadding(10, 10, 10, 10);
        row.addView(tvProgressFisik);

        return row;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
