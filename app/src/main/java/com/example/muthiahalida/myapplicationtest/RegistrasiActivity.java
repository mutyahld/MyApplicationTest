package com.example.muthiahalida.myapplicationtest;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.example.muthiahalida.myapplicationtest.model.User;
import com.example.muthiahalida.myapplicationtest.utils.ValidateUtils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class RegistrasiActivity extends AppCompatActivity {

    @BindView(R.id.et_registrasi_password)
    TextInputLayout etRegistrasiPassword;
    @BindView(R.id.et_registrasi_full_name)
    TextInputLayout etRegistrasiFullName;
    @BindView(R.id.et_registrasi_access_code)
    TextInputLayout etRegistrasiAccessCode;
    @BindView(R.id.et_email)
    TextInputLayout etEmail;
    @BindView(R.id.et_phone_number)
    TextInputLayout etPhoneNumber;
    @BindView(R.id.btn_registration)
    Button btnRegistration;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_upload_photo)
    ImageView ivUploadPhoto;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.et_registrasi_full_name_Ka_satker)
    TextInputLayout etRegistrasiFullNameKaSatker;
    @BindView(R.id.et_phone_number_ka_satker)
    TextInputLayout etPhoneNumberKaSatker;
    @BindView(R.id.ll_register)
    LinearLayout llRegister;

    String Password;
    String fullName;
    String accessCode;
    String email;
    String phoneNumber;
    String uploadPhoto;
    String photoUrl;
    String fullNameSatker;
    String fullNameKaSatker;
    String emailSatker;
    String phoneNumberKaSatker;

    private TextInputLayout[] textInputLayouts;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseUser firebaseUser;
    private FirebaseStorage firebaseStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buat Akun");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();

        textInputLayouts = new TextInputLayout[]{
                etRegistrasiPassword, etRegistrasiFullName, etRegistrasiAccessCode, etEmail, etPhoneNumber
        };

        ValidateUtils.validateInputUser(textInputLayouts, btnRegistration);

        actionButton();
        checkAccessCode();
    }

    private void getUserData() {
        Password = etRegistrasiPassword.getEditText().getText().toString();
        fullName = etRegistrasiFullName.getEditText().getText().toString();
        accessCode = etRegistrasiAccessCode.getEditText().getText().toString();
        email = etEmail.getEditText().getText().toString();
        phoneNumber = etPhoneNumber.getEditText().getText().toString();
        fullNameKaSatker = etRegistrasiFullNameKaSatker.getEditText().getText().toString();
        phoneNumberKaSatker = etPhoneNumberKaSatker.getEditText().getText().toString();
    }

    private void actionButton() {
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                getUserData();
                if (validasiPassword()) {
                    checkAccesCodeExist();
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        ivUploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(RegistrasiActivity.this)
                        .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                        .includeVideo(true) // Show video on image picker
                        .single() // single mode
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .start(); // start image picker activity with request code
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // or get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);
            String imagePath = image.getPath();
            uploadPhoto = imagePath;
            Glide.with(this).load(imagePath).into(ivUploadPhoto);
        }
    }

    private boolean validasiPassword() {
        boolean valid = true;
        if (Password != null) {
            if (Password.isEmpty() || Password.length() > 6) {
                etRegistrasiPassword.setError("Password cannot be more than 6 characters!");
                valid = false;
            } else if (Password.isEmpty() || Password.length() < 2) {
                etRegistrasiPassword.setError("Password must than 2  characters!");
                valid = false;
            } else {
                etRegistrasiPassword.setError(null);
                valid = true;
            }
        }
        return valid;
    }

    private void checkAccesCodeExist() {
        firebaseFirestore.collection("accessCode")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            QuerySnapshot doc = task.getResult();
                            String codePejabat = String.valueOf(doc.getDocuments().get(0).get("code"));
                            String codeSatker = String.valueOf(doc.getDocuments().get(1).get("code"));
                            if (accessCode.equalsIgnoreCase(codePejabat)) {
                                saveImageUser();
                            } else if (accessCode.equalsIgnoreCase(codeSatker)) {
                                saveImageUser();
                            } else {
                                Toast.makeText(RegistrasiActivity.this, "Kode akses tidak terdaftar", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    }
                });
    }

    public void createNewAccount() {
        firebaseAuth.createUserWithEmailAndPassword(email, Password)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onRegisterFailed(e.getMessage());
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            saveDataUser();
                        }
                    }
                });
    }

    private void saveImageUser() {
        if (uploadPhoto == null || uploadPhoto.isEmpty()) {
            uploadPhoto = "";
            createNewAccount();
        } else {
            Uri photoFile = Uri.fromFile(new File(uploadPhoto));
            StorageReference reference = firebaseStorage.getReference();
            final StorageReference photoUser = reference.child("PhotoUser")
                    .child(photoFile.getLastPathSegment());
            UploadTask uploadTask = photoUser.putFile(photoFile);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        onRegisterFailed(task.getException().getMessage());
                    }
                    return photoUser.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        photoUrl = task.getResult().toString();
                        createNewAccount();
                    }
                }
            });
        }

    }

    private void saveDataUser() {
        firebaseUser = firebaseAuth.getCurrentUser();
        String idUser = firebaseUser.getUid();
        User newUser = new User(idUser, email, fullName, accessCode, phoneNumber, photoUrl, fullNameKaSatker, phoneNumberKaSatker);
        firebaseFirestore.collection("User")
                .document(idUser)
                .set(newUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onRegisterSuccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onRegisterFailed(e.getMessage());
                    }
                });
    }

    public void onRegisterFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void onRegisterSuccess() {
        Toast.makeText(this, "Berhasil mendaftar", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(RegistrasiActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void checkAccessCode(){
        etRegistrasiAccessCode.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("S")) {
                    etRegistrasiFullNameKaSatker.setVisibility(View.VISIBLE);
                    etPhoneNumberKaSatker.setVisibility(View.VISIBLE);

                }
                else if (s.toString().equals("P")) {
                    etRegistrasiFullNameKaSatker.setVisibility(View.GONE);
                    etPhoneNumberKaSatker.setVisibility(View.GONE);
                }
            }
        });
    }
}