package com.example.muthiahalida.myapplicationtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class activity_menu extends AppCompatActivity {
    Button btninformasiumum;
    Button btnlaporan;
    Button btnformatraker;
    Button btnverifikasi;
    Button btnupload;
    Button btndownload;
    Button btnforumdiskusi;
    Button btnutilitas;
    Button btnakun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnlaporan = findViewById(R.id.btnlaporan);

        btnlaporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_menu.this,activity_laporan.class);
                startActivity(intent);
            }
        });

        btninformasiumum = findViewById(R.id.btninformasiumum);

        btninformasiumum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_menu.this,activity_informasiumum.class);
                startActivity(intent);
            }
        });

//        btnverifikasi = findViewById(R.id.btn_verifikasi);
//
//        btnverifikasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity_menu.this,activity_verifikasi.class);
//                startActivity(intent);
//            }
//        });

        btnupload = findViewById(R.id.btnupload);

        btnupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_menu.this,activity_upload.class);
                startActivity(intent);
            }
        });

//        btndownload = findViewById(R.id.btndownload);
//
//        btndownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity_menu.this,activity_download.class);
//                startActivity(intent);
//            }
//        });
//
//        btnutilitas = findViewById(R.id.btnutilitas);
//
//        btnutilitas.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity_menu.this,activity_utilitas.class);
//                startActivity(intent);
//            }
//        });

//        btnakun = findViewById(R.id.btnakun);
//
//        btnakun.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity_menu.this,activity_akun.class);
//                startActivity(intent);
//            }
//        });

        btnformatraker = findViewById(R.id.btnformatraker);

        btnformatraker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_menu.this,activity_formatraker.class);
                startActivity(intent);
            }
        });

//        btnforumdiskusi = findViewById(R.id.btnforumdiskusi);
//
//        btnforumdiskusi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity_menu.this,activity_forumdiskusi.class);
//                startActivity(intent);
//            }
//        });
    }
}