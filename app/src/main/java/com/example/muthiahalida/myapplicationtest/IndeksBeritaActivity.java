package com.example.muthiahalida.myapplicationtest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.muthiahalida.myapplicationtest.Adapter.IndeksBeritaAdapter;
import com.example.muthiahalida.myapplicationtest.Adapter.IndeksBeritaAdapter.OnIndeksBeritaClickListener;
import com.example.muthiahalida.myapplicationtest.model.IndeksBerita;
import java.util.ArrayList;
import java.util.List;

public class IndeksBeritaActivity extends AppCompatActivity
        implements OnIndeksBeritaClickListener {

    @BindView(R.id.rv_indeks_berita)
    RecyclerView rvIndeksBerita;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private List<IndeksBerita> indeksBeritas;

    private IndeksBeritaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indeks_berita);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Indeks Berita");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        setRvIndeksBerita();
        adapter.setOnIndeksBeritaClickListener(this);
    }

    private void setRvIndeksBerita() {
        indeksBeritas = new ArrayList<>();
        indeksBeritas.add(new IndeksBerita("Kamis, 25 Oktober 201814:50:46 WIB",
                "Undangan Rakortas Pejabat Tinggi Madya dan Pratama 05 NOVEMBER",
                "https://drive.google.com/open?id=18BiaxCpAlZc9ktYF-V79-8MTOgHw9T-G"));
        indeksBeritas.add(new IndeksBerita("Kamis, 25 Oktober 2018 14:50:46 WIB",
                "Undangan Rakortas Kepala Balai, Satker dan Pejabat Administrator Pusat 05 NOVEMBER 2018",
                "https://drive.google.com/open?id=1u6Pf0CXFU_A_q8iBk1qLrIqel00f3lUp"));

        adapter = new IndeksBeritaAdapter(this, indeksBeritas);
        rvIndeksBerita.setLayoutManager(new LinearLayoutManager(this));
        rvIndeksBerita.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvIndeksBerita.setAdapter(adapter);
    }

    @Override
    public void onIndeksBeritaClicked(IndeksBerita indeksBerita) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(indeksBerita.getFile()));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
