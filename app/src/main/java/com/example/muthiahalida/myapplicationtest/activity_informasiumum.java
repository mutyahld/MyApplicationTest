package com.example.muthiahalida.myapplicationtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class activity_informasiumum extends AppCompatActivity {
    Button btnlatarbelakang;
    Button btndasarhukum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasiumum);

        btnlatarbelakang = findViewById(R.id.btnlatarbelakang);

        btnlatarbelakang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_informasiumum.this,latarbelakang_activity.class);
                startActivity(intent);
            }
        });

        btndasarhukum = findViewById(R.id.btndasarhukum);

        btndasarhukum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_informasiumum.this,dasarhukum.class);
                startActivity(intent);
            }
        });
    }
}
