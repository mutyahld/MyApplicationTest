package com.example.muthiahalida.myapplicationtest.model;

/**
 * Created by Muthia Halida on 26/03/2019.
 */

public class Bulan {
    private String nmbulan;
    private String kdbulan;

    public Bulan() {
    }

    public Bulan(String nmbulan, String kdbulan) {
        this.nmbulan = nmbulan;
        this.kdbulan = kdbulan;
    }

    public String getNmbulan() {return nmbulan;}

    public void setNmbulan(String nmbulan) {this.nmbulan = nmbulan;}


    public String getKdbulan() {return kdbulan;}

    public void setKdbulan(String kdbulan) {this.kdbulan = kdbulan;}
}